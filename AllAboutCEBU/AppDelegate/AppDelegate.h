//
//  AppDelegate.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Global.h"
#import "NetworkLayer.h"
//#import "FacebookObject.h"
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (nonatomic, strong) AMSlideOutNavigationController *slideoutController;
//@property (nonatomic, strong) FacebookObject *facebookObjectInstance;
@property (nonatomic, copy) NSString *fbName;
@property (nonatomic, strong) CALayer *contentLayer;
@property (strong, nonatomic) FIRDatabaseReference *ref;
-(void) getAppConfig;
@end

