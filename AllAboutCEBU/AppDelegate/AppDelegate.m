//
//  AppDelegate.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "AppDelegate.h"
//#import "AMSlideOutNavigationController+Extended.h"
//#import <GoogleAnalytics-iOS-SDK/GAI.h>
#import <GoogleAnalytics/GAI.h>
#import "RootViewController.h"
#import "HomeViewController.h"
#import "SlideNavigationController.h"
#import "LeftMenuVC.h"
#import "Global.h"
#import <BuddyBuildSDK/BuddyBuildSDK.h>
#import <Firebase/Firebase.h>
#import <Rollout/Rollout.h>

@interface AppDelegate ()
@property (nonatomic,strong) LeftMenuVC *leftMenu;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [BuddyBuildSDK setup];
    [Rollout setupWithKey:@"5890c756f98bd471b0c40a4f"];
    [FIRApp configure];
    
//    [[FIRAuth auth]
//     signInAnonymouslyWithCompletion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
//         // ..
//         
//         
//         
//     }];
//
    
    
    
    [FIRDatabase database].persistenceEnabled = YES;
    
    // Override point for customization after application launch.
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.slideoutController = [AMSlideOutNavigationController slideOutNavigation];
    [self.slideoutController disableGesture];
    
    [self.slideoutController setSlideoutOptions:@{
                                                  AMOptionsEnableShadow : @(NO),
                                                  AMOptionsHeaderFont : [UIFont systemFontOfSize:14],
                                                  AMOptionsDisableMenuScroll: @(YES),
                                                  
                                                  }]; */
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    self.leftMenu = (LeftMenuVC*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    [SlideNavigationController sharedInstance].leftMenu = self.leftMenu;
    
    //[SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
    [SlideNavigationController sharedInstance].portraitSlideOffset = 90;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidClose object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Closed %@", menu);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Opened %@", menu);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidReveal object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Revealed %@", menu);
    }];
    
    //self.contentLayer = [[CALayer alloc] initWithLayer:self];
    //[self.contentLayer accessibilityActivate];
    
    
    //[self checkFacebookData];
    //[TestFlight takeOff:kTEST_FLIGHT_TOKEN];
    
     //self.facebookObjectInstance = [[FacebookObject alloc] init];
     //self.facebookObjectInstance.delegate = self;
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfileName:) name:@"updateProfileName" object:nil];
    
    // Add a second section
    
    self.fbName = @"";
    
//    UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"ProfileController"];
//    [self.slideoutController addViewControllerToLastSection:controller tagged:6 withTitle:@"PROFILE" andIcon:@""];
//    
    /*[self.slideoutController addSectionWithTitle:@"MENU"];
    self.slideoutController.title = @"";
    
    self.slideoutController.navigationItem.backBarButtonItem.title = @"";
    NSArray *controllers = [GlobalInstance loadArrayPlistFile:@"MenuControllers"];
    
    [controllers enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:[obj objectForKey:@"controller"]];
        controller.title = @"";
         controller.navigationItem.backBarButtonItem.title = @"";
        [self.slideoutController addViewControllerToLastSection:controller tagged:[[obj objectForKey:@"icon"] integerValue] withTitle:[obj objectForKey:@"title"] andIcon:[obj objectForKey:@"icon"]];
        
    }];
    
    __weak typeof(self) weakSelf = self;
    
    [self.slideoutController addActionToLastSection:^{
    
        [weakSelf.facebookObjectInstance logoutUserFromFacebook];
        
    } // Some action
                                             tagged:5
                                          withTitle:@"Logout"
                                            andIcon:@""];
    
    [self checkFacebookData];
    
    [self.slideoutController createCopywriteTxt];
    
    //[self.slideoutController createHeaderForSideMenu:nil];
    
    [self.window setRootViewController:self.slideoutController]; */
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    /*[GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:kGOOGLE_ANALYTICS_ID];*/
    
    /*return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];*/
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    
    [self getAppConfig];
    
    return YES;
}

-(void) getAppConfig {
    //Latest Listing URL
    [[[[self.ref child:@"cebu"] child:@"config"] child:@"latest"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *result = (NSDictionary *) snapshot.value;
        
        [Global sharedInstance].latestEndpointURL = [result objectForKey:@"url"];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        //[GlobalInstance showAlert:@"Unable To fetch latest endpoint URL" message:[error localizedDescription]];
    }];
    
    //Profile URL
    [[[[self.ref child:@"cebu"] child:@"config"] child:@"profile"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *result = (NSDictionary *) snapshot.value;
        
        [Global sharedInstance].profileEndpointURL = [result objectForKey:@"url"];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        //[GlobalInstance showAlert:@"Unable To fetch profile endpoint URL" message:[error localizedDescription]];
    }];
}

-(void) checkFacebookData {
    if([[NSUserDefaults standardUserDefaults] dictionaryForKey:kFBDATA]) {
        //[self.slideoutController createHeaderForSideMenu:[[NSUserDefaults standardUserDefaults] dictionaryForKey:kFBDATA]];
        
        [self.leftMenu updateContentsOnLeftMenu:[[NSUserDefaults standardUserDefaults] objectForKey:kFBDATA]];
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:[[NSUserDefaults standardUserDefaults] dictionaryForKey:kFBDATA]];
        
        //[self.slideoutController createCopywriteTxt];
        
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kSHOWREGISTRATION_FORM object:nil];
    }
}

-(void)updateProfileName:(NSNotification *)notification {
    
    //NSLog(@"updateProfileName :%@",notification.userInfo);
    
   self.fbName = @"this";
    
    //[self.slideoutController createHeaderForSideMenu:notification.object];
    
    [self.leftMenu updateContentsOnLeftMenu:notification.object];
    
    //[self.slideoutController bindDataOnView:[NSDictionary dictionaryWithObject:@"THIS IS A TEST FOR NOTIFICATION" forKey:@"Val"]];
    
    //NSLog(@"check nav controller name : %@",self.slideoutController.menuItems);
    
    
}

#pragma mark - FACEBOOK OPTIONAL DELEGATE METHODS

-(void) didLogoutFacebook:(BOOL)isLogout {
    if (isLogout) {
        //[self.slideoutController.navigationController popViewControllerAnimated:YES];
        NSIndexPath *ipath = [NSIndexPath indexPathForRow:0 inSection:0];
        //[self.slideoutController.tableView reloadData];
        //[self.slideoutController.tableView selectRowAtIndexPath:ipath animated:NO scrollPosition:UITableViewScrollPositionNone];
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kFBDATA];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kREGISTERED];
        
        if ([[NSUserDefaults standardUserDefaults] synchronize]) {
            //[[NSNotificationCenter defaultCenter] postNotificationName:kSHOWREGISTRATION_FORM object:nil];
            
            [(RootViewController *)self.window.rootViewController callLogout];
        }
        
        
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //[FBAppCall handleDidBecomeActive];
    
     //[FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - FACEBOOK Method

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    /*return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];*/

    return YES;
}

@end
