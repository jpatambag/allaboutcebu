//
//  HomeLatestEventsTableViewCell.h
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Listing.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import <FirebaseAuth/FirebaseAuth.h>
@import Firebase;

@interface HomeLatestEventsTableViewCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) FIRDatabaseReference *ref;
-(void) populateEventsData:(NSArray *)data;
-(void) retrieveEventsData;
@end
