//
//  HomeLatestEventsTableViewCell.m
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import "HomeLatestEventsTableViewCell.h"
#import "LatestActivityCollectionCell.h"
#import "Global.h"

@interface HomeLatestEventsTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *eventsObject;
-(void) populateEventsData:(NSArray *)data;
@end

@implementation HomeLatestEventsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.eventsObject = [[NSMutableArray alloc] init];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"LatestActivityCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"LatestActivityCollectionCell"];
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
}

-(void) retrieveEventsData {
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"events"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *eventsArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Events *model = [Events eventWithObject:children.value];
            [eventsArray addObject:model];
        }
       
        [self populateEventsData:eventsArray];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];

}

-(void) populateEventsData:(NSArray *)data {
    [self.collectionView registerNib:[UINib nibWithNibName:@"LatestActivityCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"LatestActivityCollectionCell"];
    [self.eventsObject addObjectsFromArray:data];
    [self.collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 Pragma Mark: UICollectionViewDelegate
 */

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.eventsObject.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LatestActivityCollectionCell *cell = (LatestActivityCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestActivityCollectionCell" forIndexPath:indexPath];
    [cell bindDataOnGridCell:[self.eventsObject objectAtIndex:indexPath.row]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(375, 250);
}

@end
