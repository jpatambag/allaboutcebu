//
//  HomeLatestListingTableViewCell.h
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Listing.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Listing.h"
#import <FirebaseAuth/FirebaseAuth.h>
@import Firebase;

@protocol HomeLatestListingTableViewCellDelegate <NSObject>
-(void) didSelectListing:(Listing *)listing;
@end

@interface HomeLatestListingTableViewCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) id <HomeLatestListingTableViewCellDelegate> delegate;
@property (strong, nonatomic) FIRDatabaseReference *ref;
-(void) populateListingData:(NSArray *)data;
-(void) retrieveListingData;
@end
