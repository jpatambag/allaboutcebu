//
//  HomeLatestListingTableViewCell.m
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import "HomeLatestListingTableViewCell.h"
#import "LatestListingCollectionCell.h"
#import "Global.h"

@interface HomeLatestListingTableViewCell () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *listingsObject;
-(void) populateListingData:(NSArray *)data;
@end


@implementation HomeLatestListingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.listingsObject = [[NSMutableArray alloc] init];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    // Initialization code
    //[self.collectionView registerClass:[LatestListingCollectionCell class] forCellWithReuseIdentifier:@"LatestListingCollectionCell"];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LatestListingCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"LatestListingCollectionCell"];
    
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    
}

-(void) retrieveListingData {
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"listing"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *listingArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Listing *model = [Listing listingWithObject:children.value];
            [listingArray addObject:model];
        }
        
        [self populateListingData:listingArray];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch listings list" message:[error localizedDescription]];
    }];

}

-(void) populateListingData:(NSArray *)data {
    [self.collectionView registerNib:[UINib nibWithNibName:@"LatestListingCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"LatestListingCollectionCell"];
    [self.listingsObject addObjectsFromArray:data];
    [self.collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 Pragma Mark: UICollectionViewDelegate
 */

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.listingsObject.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LatestListingCollectionCell *cell = (LatestListingCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestListingCollectionCell" forIndexPath:indexPath];
    [cell bindDataOnGridCell:[self.listingsObject objectAtIndex:indexPath.row]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Listing *listing = [self.listingsObject objectAtIndex:indexPath.row];
    [self.delegate didSelectListing:listing];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(375, 250);
}


@end
