//
//  HomeWeatherTableViewCell.h
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"
#import <FirebaseAuth/FirebaseAuth.h>
@import Firebase;

@interface HomeWeatherTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet UILabel *txtTitle;
@property (nonatomic,weak) IBOutlet UILabel *txtSunrise;
@property (nonatomic,weak) IBOutlet UILabel *txtSunset;
@property (nonatomic,weak) IBOutlet UILabel *txtTemp;
@property (nonatomic,weak) IBOutlet UILabel *txtHumidity;
@property (nonatomic,weak) IBOutlet UILabel *txtPressure;
@property (nonatomic,weak) IBOutlet UILabel *txtDay;
@property (nonatomic,weak) IBOutlet UILabel *txtTime;
@property (nonatomic,weak) IBOutlet UILabel *txtDate;
@property (nonatomic,weak) IBOutlet UIImageView *imgWeatherIcon;
@property (strong, nonatomic) FIRDatabaseReference *ref;
-(void) populateWeather:(Weather *) data;
-(void) retrieveWeatherData;
@end
