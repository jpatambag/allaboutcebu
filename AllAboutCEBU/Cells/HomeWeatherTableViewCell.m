//
//  HomeWeatherTableViewCell.m
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import "HomeWeatherTableViewCell.h"
#import "NSDate+Helper.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "Weather.h"
#import "Global.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"


@implementation HomeWeatherTableViewCell

-(void) populateWeather:(Weather *) data{
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    NSString *bgIcon = data.dashboardIcon;
    
    [self.imgWeatherIcon setImageWithURL:[NSURL URLWithString:bgIcon] placeholderImage:nil];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtTitle.text = data.summary;
    
    
    NSDate *sunriseTime = [NSDate dateWithTimeIntervalSince1970:[data.sunriseTime floatValue]];
    
    NSDate *sunsetTime = [NSDate dateWithTimeIntervalSince1970:[data.sunsetTime floatValue]];
    
    self.txtSunrise.text = [NSString stringWithFormat:@"Sunrise : %@",[NSDate stringFromDate:sunriseTime withFormat:@"hh:mm aa"]];
    self.txtSunset.text = [NSString stringWithFormat:@"Sunset : %@",[NSDate stringFromDate:sunsetTime withFormat:@"hh:mm aa"]];
    self.txtTemp.text = [NSString stringWithFormat:@"%d °C", [data.apparentTemperature intValue]];
    self.txtHumidity.text = [NSString stringWithFormat:@"Humidity : %@ %%",data.humidity];
    self.txtPressure.text = [NSString stringWithFormat:@"Pressure :%@ hpa",data.pressure];
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
    NSString *bgImageURL = data.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgIcon setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgIcon.image = image;
                                         
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    [self updateTime];
}

-(void) retrieveWeatherData {
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"weather"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        Weather *weatherModel = [Weather weatherWithObject:snapshot.value];
        [self populateWeather:weatherModel];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
}

-(void)updateTime
{
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
