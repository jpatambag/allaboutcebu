//
//  AboutViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 29/7/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "AboutViewController.h"
#import "Global.h"
#import "TTTAttributedLabel.h"
#import "FUICustomAuthDelegate.h"

#import <FirebaseAuthUI/FirebaseAuthUI.h>
#import <FirebaseFacebookAuthUI/FUIFacebookAuth.h>
#import <FirebaseGoogleAuthUI/FUIGoogleAuth.h>
#import <FirebaseTwitterAuthUI/FUITwitterAuth.h>
#import "FUICustomAuthPickerViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>
#import "AccountViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"

@import Firebase;

static NSString *const kFirebaseTermsOfService = @"https://firebase.google.com/terms/";
@class Firebase;

@interface AboutViewController()<TTTAttributedLabelDelegate, FUIAuthDelegate, UIWebViewDelegate>
@property (nonatomic, strong) UIButton *btnLogin;
@property (nonatomic, strong) IBOutlet UIView *loginContainer;
@property (nonatomic, strong) IBOutlet UIView *profileContainer;
@property (nonatomic, strong) IBOutlet UIImageView *profileImage;
@property (nonatomic, strong) IBOutlet UILabel *firstname;
@property (nonatomic, strong) IBOutlet UILabel *lastname;
@property (nonatomic, strong) IBOutlet UILabel *email;
@property (nonatomic, strong) IBOutlet UIWebView *profileDetails;
@property (nonatomic) FIRAuth *auth;
@property (nonatomic) FUIAuth *authUI;
// retain customAuthUIDelegate so it can be used when needed
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (nonatomic) id<FUIAuthDelegate> customAuthUIDelegate;
@property (nonatomic, assign) BOOL isCustomAuthDelegateSelected;

@property (nonatomic) FIRAuthStateDidChangeListenerHandle authStateDidChangeHandle;
@end

@implementation AboutViewController
-(void)viewDidLoad {
    [super viewDidLoad];    
    

//    self.btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.btnLogin.frame = CGRectMake(self.view.frame.origin.x / 2, self.view.frame.origin.y / 2, 100, 40);
//    [self.btnLogin addTarget:self action:@selector(popUpLoginController) forControlEvents:UIControlEventTouchUpInside];
//    [self.btnLogin setBackgroundColor:[UIColor redColor]];
//    [self.view addSubview:self.btnLogin];
    
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    
    self.customAuthUIDelegate = [[FUICustomAuthDelegate alloc] init];
    
    self.auth = [FIRAuth auth];
    self.authUI = [FUIAuth defaultAuthUI];
    
    self.authUI.TOSURL = [NSURL URLWithString:kFirebaseTermsOfService];
    
    //set AuthUI Delegate
    self.authUI.delegate = self.customAuthUIDelegate;
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    /*NSRange r = [self.tttLabel.text rangeOfString:@"http://www.AllAboutCebu.com"];
    [self.tttLabel addLinkToURL:[NSURL URLWithString:@"action://show-site"] withRange:r];*/
}

-(void) viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
    self.profileContainer.hidden = YES;
    __weak FUIAuthBaseViewController *weakSelf = self;
    self.authStateDidChangeHandle = [self.auth addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        
        NSLog(@"user UID :%@",user.uid);
        NSLog(@"user photo url : %@",user.photoURL);
        NSLog(@"addAuthStateDidChangeListener :%@",user.email);
        NSLog(@"addAuthStateDidChangeListener :%@",user.providerID);
        
        
        if ([user.uid length] == 0) {
            [self showLoginView];
        } else {
            [self saveUser:user];
            [self updateProfileContent:user];
        }
        
    }];
}

-(void) saveUser:(FIRUser *) user {
    
    [[[_ref child:@"users"] child:user.uid]
     setValue:@{@"email": user.email,
                @"uid":user.uid}];
}

-(void) showLoginView {
    _loginContainer.hidden = NO;
    _profileContainer.hidden = YES;
    _loginContainer.userInteractionEnabled = YES;
    _profileContainer.userInteractionEnabled = NO;
    self.navigationController.navigationBarHidden = YES;
    [self.view sendSubviewToBack:_profileContainer];
    [self.view bringSubviewToFront:_loginContainer];
}

-(void) updateProfileContent:(FIRUser *) user {
    
    _loginContainer.hidden = YES;
    _profileContainer.hidden = NO;
    _loginContainer.userInteractionEnabled = NO;
    _profileContainer.userInteractionEnabled = YES;
    self.navigationController.navigationBarHidden = NO;
    [self.view sendSubviewToBack:_loginContainer];
    [self.view bringSubviewToFront:_profileContainer];
    
    UIBarButtonItem *logoutBtn = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style: UIBarButtonItemStylePlain target:self action:@selector(logout)];
    
    //self.navigationController.navigationItem.rightBarButtonItem = logoutBtn;
    
    self.navigationItem.rightBarButtonItem = logoutBtn;
    
    self.profileDetails.frame = CGRectMake(0,
                                           64,
                                           self.profileDetails.frame.size.width,
                                           self.profileDetails.frame.size.height);
    
    NSLog(@"PROFILE URL: %@%@",KURL_ENDPOINT,user.uid);
    
    _firstname.text = user.displayName;
    
    NSString *sourceURL = [Global sharedInstance].profileEndpointURL;
    
    if (![sourceURL isEqualToString:@""]) {
       [self.profileDetails loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",sourceURL,user.uid]]]];
    }
    
    
   /* [self.profileDetails loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KURL_ENDPOINT,user.uid]]]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:user.photoURL];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.profileImage setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.profileImage.image = image;
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];*/
    
    

}

-(void) logout {
    NSError *error;
    [self.authUI signOutWithError:&error];
    [self showLoginView];
    if (error) {
        [self showAlert:error.localizedDescription];
    }
}

- (IBAction)onAuthorization:(id)sender {
    if (!self.auth.currentUser) {
        
        _authUI.providers = [self getListOfIDPs];
        
        UINavigationController *controller = [self.authUI authViewController];
        controller.navigationBar.hidden = YES;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        [self signOut];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.auth removeAuthStateDidChangeListener:self.authStateDidChangeHandle];
    }

-(IBAction)onBtnShowSite:(id)sender {
    
}

- (void)updateUI:(FIRAuth * _Nonnull) auth withUser:(FIRUser * _Nullable) user {
    if (user) {
//        self.cellSignIn.textLabel.text = @"Signed-in";
//        self.cellName.textLabel.text = user.displayName;
//        self.cellEmail.textLabel.text = user.email;
//        self.cellUID.textLabel.text = user.uid;
//        
//        self.buttonAuthorization.title = @"Sign Out";
    } else {
//        self.cellSignIn.textLabel.text = @"Not signed-in";
//        self.cellName.textLabel.text = @"";
//        self.cellEmail.textLabel.text = @"";
//        self.cellUID.textLabel.text = @"";
//        
//        self.buttonAuthorization.title = @"Sign In";
    }
    
    //self.cellAccessToken.textLabel.text = [self getAllAccessTokens];
    //self.cellIdToken.textLabel.text = [self getAllIdTokens];
    
//    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
//    [self.tableView reloadData];
//    for (NSIndexPath *path in selectedRows) {
//        [self.tableView selectRowAtIndexPath:path
//                                    animated:NO
//                              scrollPosition:UITableViewScrollPositionNone];
//    }
}
- (IBAction)onAuthUIDelegateChanged:(UISwitch *)sender {
    _isCustomAuthDelegateSelected = sender ? sender.isOn : NO;
    if (_isCustomAuthDelegateSelected) {
        
    } else {
        self.authUI.delegate = self;
    }
}

//- (IBAction)onAuthorization:(id)sender {
//    if (!self.auth.currentUser) {
//        
//        //_authUI.providers = [self getListOfIDPs];
//        //_authUI.signInWithEmailHidden = ![self isEmailEnabled];
//        
//        UINavigationController *controller = [self.authUI authViewController];
//        if (_isCustomAuthDelegateSelected) {
//            controller.navigationBar.hidden = YES;
//        }
//        [self presentViewController:controller animated:YES completion:nil];
//    } else {
//        [self signOut];
//    }
//}

#pragma mark - FUIAuthDelegate methods

// this method is called only when FUIAuthViewController is delgate of AuthUI
- (void)authUI:(FUIAuth *)authUI didSignInWithUser:(nullable FIRUser *)user error:(nullable NSError *)error {
    if (error) {
        if (error.code == FUIAuthErrorCodeUserCancelledSignIn) {
            [self showAlert:@"User cancelled sign-in"];
        } else {
            NSError *detailedError = error.userInfo[NSUnderlyingErrorKey];
            if (!detailedError) {
                detailedError = error;
            }
            [self showAlert:detailedError.localizedDescription];
        }
    }
}

#pragma mark - Helper Methods

- (NSString *)getAllAccessTokens {
    NSMutableString *result = [NSMutableString new];
    for (id<FUIAuthProvider> provider in _authUI.providers) {
        [result appendFormat:@"%@:  %@\n", provider.shortName, provider.accessToken];
    }
    
    return result;
}

- (NSString *)getAllIdTokens {
    NSMutableString *result = [NSMutableString new];
    for (id<FUIAuthProvider> provider in _authUI.providers) {
        [result appendFormat:@"%@:  %@\n", provider.shortName, provider.idToken];
    }
    
    return result;
}

- (void)signOut {
    NSError *error;
    [self.authUI signOutWithError:&error];
    if (error) {
        [self showAlert:error.localizedDescription];
    }
}

- (void)showAlert:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* closeButton = [UIAlertAction
                                  actionWithTitle:@"Close"
                                  style:UIAlertActionStyleDefault
                                  handler:nil];
    [alert addAction:closeButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (NSArray *)getListOfIDPs {
    NSMutableArray *providers = [NSMutableArray new];
    id<FUIAuthProvider> emailProvider;
    //id<FUIAuthProvider> googleProvider;
    //id<FUIAuthProvider> facebookProvider;
    
    emailProvider =  [[FUIGoogleAuth alloc] initWithScopes:@[kGoogleUserInfoEmailScope,
                                                             kGoogleUserInfoProfileScope,
                                                             kGoogleGamesScope,
                                                             kGooglePlusMeScope]];
    //facebookProvider = [[FUIFacebookAuth alloc] initWithPermissions:<#(nonnull NSArray *)#>];
    //googleProvider =  [[FUIGoogleAuth alloc] init];
    [providers addObject:emailProvider];
    
    return providers;
}


@end
