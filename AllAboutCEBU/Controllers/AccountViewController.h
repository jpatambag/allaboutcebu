//
//  AccountViewController.h
//  AllAboutCEBU
//
//  Created by Joseph on 12/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountViewController : UIViewController
@property (nonatomic, strong) FIRUser *userObj;
@property (nonatomic, strong) FIRAuth *firAuth;
- (void)updateUI:(FIRAuth * _Nonnull) auth withUser:(FIRUser * _Nullable) user;
@end
