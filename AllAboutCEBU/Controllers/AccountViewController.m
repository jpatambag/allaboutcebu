//
//  AccountViewController.m
//  AllAboutCEBU
//
//  Created by Joseph on 12/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

@import Firebase;

#import "AccountViewController.h"

#import "FUICustomAuthDelegate.h"
#import <FirebaseAuthUI/FirebaseAuthUI.h>
#import <FirebaseFacebookAuthUI/FUIFacebookAuth.h>
#import <FirebaseGoogleAuthUI/FUIGoogleAuth.h>
#import <FirebaseTwitterAuthUI/FUITwitterAuth.h>

#import "FUICustomAuthPickerViewController.h"

typedef enum : NSUInteger {
    kSectionsSettings = 0,
    kSectionsProviders,
    kSectionsName,
    kSectionsEmail,
    kSectionsUID,
    kSectionsAccessToken,
    kSectionsIDToken
} UISections;

typedef enum : NSUInteger {
    kIDPEmail = 0,
    kIDPGoogle,
    kIDPFacebook,
    kIDPTwitter
} FIRProviders;

static NSString *const kFirebaseTermsOfService = @"https://firebase.google.com/terms/";

@interface AccountViewController () <FUIAuthDelegate>
@property (nonatomic) FIRAuth *auth;
@property (nonatomic) FUIAuth *authUI;
@property (nonatomic) id<FUIAuthDelegate> customAuthUIDelegate;
@property (nonatomic, assign) BOOL isCustomAuthDelegateSelected;
@property (nonatomic) FIRAuthStateDidChangeListenerHandle authStateDidChangeHandle;

@property (nonatomic, strong) IBOutlet UIImageView *profileImage;
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblEmail;

@end



@interface AccountViewController ()

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.customAuthUIDelegate = [[FUICustomAuthDelegate alloc] init];
    
    self.auth = [FIRAuth auth];
    self.authUI = [FUIAuth defaultAuthUI];
    
    self.authUI.TOSURL = [NSURL URLWithString:kFirebaseTermsOfService];
    
    //set AuthUI Delegate
    [self onAuthUIDelegateChanged:nil];
    //[self updateUI:self.firAuth withUser:self.userObj];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    __weak AccountViewController *weakSelf = self;
    self.authStateDidChangeHandle = [self.auth addAuthStateDidChangeListener:^(FIRAuth * _Nonnull auth, FIRUser * _Nullable user) {
        [weakSelf updateUI:auth withUser:user];
    }];
    
    self.navigationController.toolbarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.auth removeAuthStateDidChangeListener:self.authStateDidChangeHandle];
    
    self.navigationController.toolbarHidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI methods

- (void)updateUI:(FIRAuth * _Nonnull) auth withUser:(FIRUser * _Nullable) user {
    if (user) {
//        self.cellSignIn.textLabel.text = @"Signed-in";
//        self.cellName.textLabel.text = user.displayName;
//        self.cellEmail.textLabel.text = user.email;
//        self.cellUID.textLabel.text = user.uid;
//        
//        self.buttonAuthorization.title = @"Sign Out";
    } else {
//        self.cellSignIn.textLabel.text = @"Not signed-in";
//        self.cellName.textLabel.text = @"";
//        self.cellEmail.textLabel.text = @"";
//        self.cellUID.textLabel.text = @"";
//        
//        self.buttonAuthorization.title = @"Sign In";
    }
    
    //self.cellAccessToken.textLabel.text = [self getAllAccessTokens];
    //self.cellIdToken.textLabel.text = [self getAllIdTokens];
    
}
- (IBAction)onAuthUIDelegateChanged:(UISwitch *)sender {
    _isCustomAuthDelegateSelected = sender ? sender.isOn : NO;
    if (_isCustomAuthDelegateSelected) {
        self.authUI.delegate = self.customAuthUIDelegate;
    } else {
        self.authUI.delegate = self;
    }
}

#pragma mark - FUIAuthDelegate methods

// this method is called only when FUIAuthViewController is delgate of AuthUI
- (void)authUI:(FUIAuth *)authUI didSignInWithUser:(nullable FIRUser *)user error:(nullable NSError *)error {
    if (error) {
        if (error.code == FUIAuthErrorCodeUserCancelledSignIn) {
            [self showAlert:@"User cancelled sign-in"];
        } else {
            NSError *detailedError = error.userInfo[NSUnderlyingErrorKey];
            if (!detailedError) {
                detailedError = error;
            }
            [self showAlert:detailedError.localizedDescription];
        }
    }
}

#pragma mark - Helper Methods

- (NSString *)getAllAccessTokens {
    NSMutableString *result = [NSMutableString new];
    for (id<FUIAuthProvider> provider in _authUI.providers) {
        [result appendFormat:@"%@:  %@\n", provider.shortName, provider.accessToken];
    }
    
    return result;
}

- (NSString *)getAllIdTokens {
    NSMutableString *result = [NSMutableString new];
    for (id<FUIAuthProvider> provider in _authUI.providers) {
        [result appendFormat:@"%@:  %@\n", provider.shortName, provider.idToken];
    }
    
    return result;
}

- (void)signOut {
    NSError *error;
    [self.authUI signOutWithError:&error];
    if (error) {
        [self showAlert:error.localizedDescription];
    }
}

- (void)showAlert:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* closeButton = [UIAlertAction
                                  actionWithTitle:@"Close"
                                  style:UIAlertActionStyleDefault
                                  handler:nil];
    [alert addAction:closeButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
