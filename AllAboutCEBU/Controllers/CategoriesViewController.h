//
//  CategoriesViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 9/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
@interface CategoriesViewController : UICollectionViewController
@property (strong, nonatomic) FIRDatabaseReference *ref;
@end
