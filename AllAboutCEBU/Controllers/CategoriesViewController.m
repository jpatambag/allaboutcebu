//
//  CategoriesViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 9/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoryCell.h"
#import "NetworkLayer.h"
#import "Constants.h"
#import "ListingViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "LoadMoreTableFooterView.h"
#import "SlideNavigationController.h"
#import "SearchPlaceController.h"
#import "CategoryCollectionCell.h"
#import "AllAboutCEBU-swift.h"
#import "SearchViewController.h"
#import "Global.h"
#import "Categories.h"

@interface CategoriesViewController () <NetworkLayerDelegate, EGORefreshTableHeaderDelegate, LoadMoreTableFooterDelegate,SlideNavigationControllerDelegate, UICollectionViewDataSource, CollectionViewWaterfallLayoutDelegate> {
    
    NetworkLayer *network;
    
    EGORefreshTableHeaderView *egoRefreshTableHeaderView;
    BOOL isRefreshing;
    
    LoadMoreTableFooterView *loadMoreTableFooterView;
    BOOL isLoadMoreing;
    
    int dataRows;
    
    int page;
    
}
@property (nonatomic,strong) NSMutableArray *tableData;
@property (nonatomic,strong) SearchViewController *searchController;
@end

@implementation CategoriesViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    self.tableData = [[NSMutableArray alloc] init];
    
    page = 1;
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
    
   
    CollectionViewWaterfallLayout *layout = [[CollectionViewWaterfallLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.headerHeight = 0;
    layout.footerHeight = 0;
    layout.minimumColumnSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    self.collectionView.collectionViewLayout = layout;
    
    // Register cell classes
    UINib *cellNib = [UINib nibWithNibName:@"CategoryCollectionCell" bundle:nil];
    //[self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"CategoryCollectionCell"];
    
    //[self.collectionView registerClass:[CategoryCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"CategoryCollectionCell"];
    
    [self.collectionView registerClass:UICollectionReusableView.self forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Header"];
    
    [self.collectionView registerClass:UICollectionReusableView.self forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"Footer"];
    
    
    self.title = @"Places";
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    
    [self getDataFromAPIWithPageNo:page];
    
    // Do any additional setup after loading the view.
}

-(void) getDataFromAPIWithPageNo:(int) index {
    //[network requestDataFromServer:kGET_LIST_OF_CATEGORY andParams:nil forTag:0];
    
    [[[self.ref child:@"cebu"] child:@"categories"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *categoriesArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Categories *model = [Categories categoryWithObject:children.value];
            [categoriesArray addObject:model];
        }
        
        [self.tableData removeAllObjects];
        [self.tableData addObjectsFromArray:categoriesArray];
        
        [self.collectionView reloadData];

        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];

}

-(void)serverResponse:(id)response tag:(int)apiTag {
    //[self.tableData removeAllObjects];
    
    page += 1;
    
    [self.tableData addObjectsFromArray:(NSArray *)response];
    
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.tableData count];
}

- (CategoryCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    CategoryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionCell" forIndexPath:indexPath];
    [cell updateCellContent:[self.tableData objectAtIndex:indexPath.row]];
    return cell;
}

- (void)reloadData
{
    [self.collectionView reloadData];
    
    loadMoreTableFooterView.frame = CGRectMake(0.0f, self.collectionView.contentSize.height, self.view.frame.size.width, self.collectionView.bounds.size.height);
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidScroll:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidEndDragging:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    isRefreshing = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        //dataRows = 20; // show first 20 records after refreshing
        [self getDataFromAPIWithPageNo:1];
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete refreshing
            isRefreshing = NO;
            [self reloadData];
            
            [egoRefreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.collectionView];
        });
    });
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return isRefreshing;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

#pragma mark LoadMoreTableFooterDelegate Methods

- (void)loadMoreTableFooterDidTriggerLoadMore:(LoadMoreTableFooterView*)view
{
    isLoadMoreing = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        [self getDataFromAPIWithPageNo:page];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete loading...
            isLoadMoreing = NO;
            
            [self reloadData];
            
            [loadMoreTableFooterView loadMoreScrollViewDataSourceDidFinishedLoading:self.collectionView];
        });
    });
}
- (BOOL)loadMoreTableFooterDataSourceIsLoading:(LoadMoreTableFooterView*)view
{
    return isLoadMoreing;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(109, 100);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Places" bundle:nil];
    self.searchController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    
    Categories *model = (Categories *)[self.tableData objectAtIndex:indexPath.row];
    self.searchController.category = model.name;
    //NSLog(@"OBject selected :%@",[self.tableData objectAtIndex:indexPath.row]);
    
    //self.searchController.placesObject = [self.tableData objectAtIndex:indexPath.row];
    //self.listingDetails.dataDict = [self.tableData objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:self.searchController animated:YES];
}




#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
