//
//  CurrentWeatherViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 24/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "CurrentWeatherViewController.h"

@interface CurrentWeatherViewController ()
@property (nonatomic, strong) IBOutlet UIImageView *weatherBg;
@property (nonatomic, strong) IBOutlet UILabel *lblTemperature;
@end

@implementation CurrentWeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
