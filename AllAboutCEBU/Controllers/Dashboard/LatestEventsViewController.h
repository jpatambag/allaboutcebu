//
//  LatestEventsViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 14/12/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"

@protocol LatestEventsViewControllerDelegate <NSObject>
-(void) didTapShowEventDetail:(NSDictionary *)data;
@end

@interface LatestEventsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate,GMGridViewActionDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) IBOutlet GMGridView *carousel;
@property (nonatomic, strong) id <LatestEventsViewControllerDelegate> delegate;
@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
-(void) populateDataOnView:(NSArray *) array;
@end
