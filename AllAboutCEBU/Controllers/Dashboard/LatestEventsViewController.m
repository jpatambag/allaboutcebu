//
//  LatestEventsViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 14/12/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "LatestEventsViewController.h"
#import "LatestActivityGridCell.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "Global.h"
#import "Constants.h"
#import "LatestActivityCollectionCell.h"


@interface LatestEventsViewController ()
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation LatestEventsViewController
#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //configure carousel
    //self.carousel.type = iCarouselTypeCoverFlow2;

    self.carousel.style = GMGridViewStylePush;
    self.carousel.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontalPagedLTR];
    self.carousel.itemSpacing = 0;
    self.carousel.minEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.carousel.showsHorizontalScrollIndicator = NO;
    self.carousel.showsVerticalScrollIndicator = NO;
    self.carousel.centerGrid = YES;
    self.carousel.actionDelegate = self;
    //self.title = @"Dashboard";
    //self.items = [[NSMutableArray alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

-(void) populateDataOnView:(NSArray *) array{
    
    if (!self.items) {
        self.items = [[NSMutableArray alloc] initWithArray:array];
        
        
    } else {
        [self.items removeAllObjects];
        
        [self.items addObjectsFromArray:array];
        
    }
    
     //[self.carousel reloadData];
    [self.collectionView reloadData];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - GMGridViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////////////////
- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index {
    
    LatestActivityGridCell *cell = (LatestActivityGridCell*) [gridView dequeueReusableCellWithIdentifier:@"LatestActivityGridCell"];
    
    if (!cell) {
        cell = [LatestActivityGridCell gridCell];
    }
    
    //[cell bindDataOnGridCell:[self.items objectAtIndex:(long)index]];
    return cell;
    
}

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView {

    return [self.items count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation {
    //return CGSizeMake(380, 119);
    
    //return CGSizeMake(280, 119);
    
    CGSize contentSize;
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        contentSize = CGSizeMake(280, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        contentSize = CGSizeMake(380, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        contentSize = CGSizeMake(380, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        contentSize = CGSizeMake(280, 250);
    }
    
    return contentSize;
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    //NSLog(@"INDEX POSITION :%ld",position);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapShowEventDetail:)]) {
        [_delegate didTapShowEventDetail:[self.items objectAtIndex:(long)position]];
    }
    
}

#pragma mark UICOllectionView Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LatestActivityCollectionCell *myCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"LatestActivityCollectionCell"
                                    forIndexPath:indexPath];

    [myCell bindDataOnGridCell:[self.items objectAtIndex:indexPath.row]];
    
    return myCell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize contentSize;
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        contentSize = CGSizeMake(280, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        contentSize = CGSizeMake(380, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        contentSize = CGSizeMake(414, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        contentSize = CGSizeMake(280, 250);
    }

    return contentSize;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapShowEventDetail:)]) {
        [_delegate didTapShowEventDetail:[self.items objectAtIndex:indexPath.row]];
    }
}

@end
