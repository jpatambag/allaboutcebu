//
//  LatestListingsViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 13/12/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"

@protocol LatestListingsViewControllerDelegate <NSObject>
-(void) didTapShowListingDetail:(NSDictionary *)data;
@end

@interface LatestListingsViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, GMGridViewActionDelegate>
@property (nonatomic, strong) IBOutlet GMGridView *carousel;
@property (nonatomic, strong) id <LatestListingsViewControllerDelegate> delegate;
-(void) populateDataOnView:(NSArray *) array;

@end
