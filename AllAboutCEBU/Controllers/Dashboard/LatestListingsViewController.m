//
//  LatestListingsViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 13/12/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "LatestListingsViewController.h"
#import "Global.h"
#import "Constants.h"
#import "LatestListingGridCell.h"

@interface LatestListingsViewController ()
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation LatestListingsViewController
#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //configure carousel
    //self.carousel.type = iCarouselTypeCoverFlow2;
    
    self.carousel.style = GMGridViewStylePush;
    self.carousel.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontalPagedLTR];
    self.carousel.itemSpacing = 0;
    self.carousel.minEdgeInsets = UIEdgeInsetsMake(0 ,0, 0, 0);
    self.carousel.showsHorizontalScrollIndicator = NO;
    self.carousel.showsVerticalScrollIndicator = NO;
    self.carousel.centerGrid = NO;
    self.carousel.actionDelegate = self;
    //self.title = @"Dashboard";
    //self.items = [[NSMutableArray alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

-(void) populateDataOnView:(NSArray *) array{
    NSLog(@"populateDataOnView :%@",array);
    if (!self.items) {
        self.items = [[NSMutableArray alloc] initWithArray:array];
        
        
    } else {
        [self.items removeAllObjects];
        
        [self.items addObjectsFromArray:array];
        
    }
    
    [self.carousel reloadData];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - GMGridViewDataSource
/////////////////////////////////////////////////////////////////////////////////////////////////////
- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index {
    
    LatestListingGridCell *cell = (LatestListingGridCell*) [gridView dequeueReusableCellWithIdentifier:@"LatestListingGridCell"];
    
    if (!cell) {
        cell = [LatestListingGridCell gridCell];
    }
    
    [cell bindDataOnGridCell:[self.items objectAtIndex:(long)index]];
    return cell;
    
}

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView {
    
    return [self.items count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation {
    //return CGSizeMake(380, 119);
    
    //return CGSizeMake(280, 119);
    
    CGSize contentSize;
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        contentSize = CGSizeMake(280, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        contentSize = CGSizeMake(380, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        contentSize = CGSizeMake(414, 250);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        contentSize = CGSizeMake(280, 250);
    }
    
    return contentSize;
}

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapShowListingDetail:)]) {
        [_delegate didTapShowListingDetail:[self.items objectAtIndex:(long)position]];
    }
    
}

@end
