//
//  LatestWeatherViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LatestWeatherViewControllerDelegate <NSObject>
-(void) didTapShowWeatherDetail:(NSDictionary *)data;
@end

@interface LatestWeatherViewController : UIViewController
//-(void) populateDataOnView:(NSArray *) array;
-(void) populateDataOnView:(NSMutableDictionary *) data;
@property (nonatomic, strong) id <LatestWeatherViewControllerDelegate> delegate;
@end
