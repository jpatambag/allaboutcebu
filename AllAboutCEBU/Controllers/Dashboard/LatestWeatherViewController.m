//
//  LatestWeatherViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LatestWeatherViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"

@interface LatestWeatherViewController ()
@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet UILabel *txtTitle;
@property (nonatomic,weak) IBOutlet UILabel *txtSunrise;
@property (nonatomic,weak) IBOutlet UILabel *txtSunset;
@property (nonatomic,weak) IBOutlet UILabel *txtTemp;
@property (nonatomic,weak) IBOutlet UILabel *txtHumidity;
@property (nonatomic,weak) IBOutlet UILabel *txtPressure;
@property (nonatomic,weak) IBOutlet UILabel *txtDay;
@property (nonatomic,weak) IBOutlet UILabel *txtTime;
@property (nonatomic,weak) IBOutlet UILabel *txtDate;
@property (nonatomic,weak) IBOutlet UIImageView *imgWeatherIcon;
@property (nonatomic,strong) NSMutableDictionary *weatherData;
@end

@implementation LatestWeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!self.weatherData) {
        self.weatherData = [[NSMutableDictionary alloc] init];
    }
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
}

-(void)updateTime
{
    
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
}

-(void) populateDataOnView:(NSMutableDictionary *) data{
    
    [self.weatherData removeAllObjects];
    //[self.weatherData addObjectsFromArray:array];
    [self.weatherData setDictionary:data];
    
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    NSString *imageURL = [NSString stringWithFormat:@"%@/%@.png",kWEATHER_IMAGE_ICON,[self.weatherData objectForKey:@"icon"]];
    [self.imgWeatherIcon setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@""]];
    
    self.txtTitle.text = [self.weatherData objectForKey:@"summary"];
    //self.txtSunrise.text = [NSString stringWithFormat:@"Sunrise : %@",[NSDate stringFromDate:[NSDate dateFromString:[self.weatherData objectForKey:@"sys_sunrise"]] withFormat:@"hh:mm aa"]];
    //self.txtSunset.text = [NSString stringWithFormat:@"Sunset : %@",[NSDate stringFromDate:[NSDate dateFromString:[self.weatherData objectForKey:@"sys_sunset"]] withFormat:@"hh:mm aa"]];
    self.txtTemp.text = [NSString stringWithFormat:@"%0.1f°C", [[self.weatherData objectForKey:@"temperature"] floatValue]];
    self.txtHumidity.text = [NSString stringWithFormat:@"Humidity : %@ %%",[self.weatherData objectForKey:@"humidity"]];
    self.txtPressure.text = [NSString stringWithFormat:@"Pressure :%@ hpa",[self.weatherData objectForKey:@"pressure"]];
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
    NSString *bgImageURL = [NSString stringWithFormat:@"%@%@/weather_image.png",ROOT_URL,kGET_DASHBOARD_WEATHER];
//    [self.imgIcon setImageWithURL:[NSURL URLWithString:bgImageURL] placeholderImage:[UIImage imageNamed:@""]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgIcon setImageWithURLRequest:request
                               placeholderImage:[UIImage imageNamed:@"placeholder"]
                                        success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                //weakSelf.imgIcon.image = image;
                                                
                                                
                                                
                                            });
                                            
                                        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                            //NSLog(@"failed loading image: %@", error);
                                        }];

}

-(IBAction)onBtnShowWeatherDetails:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(didTapShowWeatherDetail:)]) {
        [_delegate didTapShowWeatherDetail:self.weatherData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
