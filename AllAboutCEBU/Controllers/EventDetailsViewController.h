//
//  EventDetailsViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 26/1/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Events.h"

@class GADBannerView;
@interface EventDetailsViewController : UIViewController
@property (nonatomic, strong) Events *event;
@end
