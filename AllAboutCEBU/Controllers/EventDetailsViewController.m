//
//  EventDetailsViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 26/1/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

@import GoogleMobileAds;

#import "EventDetailsViewController.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Global.h"
#import <MapKit/MapKit.h>
#define METERS_PER_MILE 1609.344

@interface EventDetailsViewController () <GADBannerViewDelegate,MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblVenue;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
@property (strong, nonatomic) IBOutlet UITextView *txtDesc;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placeHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dateHeightConstrain;
@property (strong, nonatomic) IBOutlet GADBannerView *googleBannerVIew;
@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    //self.scrollView.contentSize = CGSizeMake(<#CGFloat width#>, <#CGFloat height#>)
    [self addBannerOnThisView];
}

-(void) addBannerOnThisView {
    self.googleBannerVIew.adUnitID = kGOOGLE_BANNER_ADD_UNIT;
    self.googleBannerVIew.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    self.googleBannerVIew.delegate = self;
    // Enable test ads on simulators.
    request.testDevices = @[@"d1c340abb06de75c6a29e08865770c8be343e606",@"c1589702d7b51e99ba56a98c3ccad9b85f75109f",@"59daea62306d7c61bbb77cb637e1e389b21ee23c"];
    [self.googleBannerVIew loadRequest:request];
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self populateDataOnView];
    [self showMapWithPins];
}

-(void) showMapWithPins {
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [_event.lat floatValue];
    zoomLocation.longitude= [_event.lng floatValue];
    
    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
    myAnnotation.coordinate = zoomLocation;
    myAnnotation.title = _event.name;
    myAnnotation.subtitle = _event.venue;
    [self.mapView addAnnotation:myAnnotation];
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];

    
    /*CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [[self.dataDict objectForKey:@"lat"] floatValue];
    zoomLocation.longitude= [[self.dataDict objectForKey:@"lng"] floatValue];
    
    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
    myAnnotation.coordinate = zoomLocation;
    myAnnotation.title = [self.dataDict objectForKey:@"Name"];
    myAnnotation.subtitle = [self.dataDict objectForKey:@"Address"];
    [self.mapView addAnnotation:myAnnotation];
    
    // 2
    //MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5* METERS_PER_MILE);
    
    // 3
    //[_mapView setRegion:viewRegion animated:YES];
    
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (zoomLocation, 2000, 2000);
    //    [_mapView setRegion:region animated:YES];
    
    MKCoordinateRegion region;
    region.center.latitude = [[self.dataDict objectForKey:@"lat"] floatValue];
    region.center.longitude = [[self.dataDict objectForKey:@"lng"] floatValue];
    //region.span.latitudeDelta = 0.112872;
    //region.span.longitudeDelta = 0.109863;
    [self.mapView setRegion:region animated:YES]; */
}

-(void)adjustScrollViewContentSize {
    
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 314);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 218);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 818);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 395);
    }
}


-(void) populateDataOnView {
    
    [self.imgItem setImage:[UIImage imageNamed:@"placeholder.png"]];
    
    //NSDictionary *filter = [self.dataDict dictionaryByReplacingNullsWithBlanks];
    
    //NSLog(@"filter: %@", filter);
    
    /*if ([[filter objectForKey:@"DateTime"] length] == 0) {
        self.lblDateTime.text = @"No Date";
    } else {
        NSDate *dateinitial = [NSDate dateFromString:[filter objectForKey:@"DateTime"]];
        NSString *otherDateString = [NSDate stringFromDate:dateinitial withFormat:@"EEE, dd MMM yyyy @ hh:mm aa"];
        self.lblDateTime.text = otherDateString;
    }*/
    
    /*NSString *imageURL = [NSString stringWithFormat:@"%@%@%@",ROOT_URL,kIMAGE_URL,[filter objectForKey:@"ImageGallery"]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *imageURL = [NSString stringWithFormat:@"%@%@%@",ROOT_URL,kIMAGE_URL_PATH,[responseObject objectForKey:@"Name"]];
        [self.imgItem setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        //[self adjustScrollViewContentSize];
        [self resetAutoLayoutConstraints];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"Image error: %@", error);
    }];
    [operation start];*/
    
    /*self.title = [filter objectForKey:@"Title"];
    
    self.lblTitle.text = [filter objectForKey:@"Title"];
    self.lblVenue.text = [filter objectForKey:@"Venue"];
    self.txtDesc.text = [filter objectForKey:@"Description"];*/
    
    if ([_event.date length] == 0) {
        self.lblDateTime.text = @"No Date";
    } else {
        NSDate *dateinitial = [NSDate dateFromString:_event.date];
        NSString *otherDateString = [NSDate stringFromDate:dateinitial withFormat:@"EEE, dd MMM yyyy @ hh:mm aa"];
        self.lblDateTime.text = otherDateString;
    }
    
    self.lblTitle.text = _event.name;
    self.lblVenue.text = _event.desc;
    
    NSString *bgImageURL = _event.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgItem.image = image;
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];
    
    
    [self resetAutoLayoutConstraints];
    
    //[self.txtDesc sizeToFit];
    
//    self.txtDesc.font = [UIFont fontLight:22];
//    self.txtDesc.textColor = [UIColor whiteColor];
//    self.txtDesc.textAlignment = NSTextAlignmentCenter;
//    _constraintTextViewHeight.constant = ceilf([self.txtDesc sizeThatFits:CGSizeMake(self.txtDesc.frame.size.width, FLT_MAX)].height);
//    
//    [self.txtDesc layoutIfNeeded];
//    [self.txtDesc updateConstraints];
}

-(void)resetAutoLayoutConstraints {
    CGSize sizeThatFitsTextView = [self.txtDesc sizeThatFits:CGSizeMake(self.txtDesc.frame.size.width, MAXFLOAT)];
    
    self.textViewHeightConstrain.constant = sizeThatFitsTextView.height;
    self.txtDesc.frame = CGRectMake(13, 342, 379, sizeThatFitsTextView.height);
    //self.textViewHeightConstraint.constant = ceilf(sizeThatFits.height);
    
    //[self adjustScrollViewContentSize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ADBanner Delegate

- (void) adView: (GADBannerView*) view didFailToReceiveAdWithError: (GADRequestError*) error {
    
}

- (void) adViewDidReceiveAd: (GADBannerView*) view {
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
