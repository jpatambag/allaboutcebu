//
//  HomeViewController.m
//  AllAboutCEBU
//
//  Created by Joseph on 22/1/17.
//  Copyright © 2017 Joseph Patambag. All rights reserved.
//

#import "HomeViewController.h"
#import "Global.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "WeatherController.h"
#import "EventsController.h"
#import "SearchPlaceController.h"
#import "NetworkLayer.h"
#import "LatestListingsViewController.h"
#import "LatestEventsViewController.h"
#import "EventDetailsViewController.h"
#import "ListingDetailsViewController.h"
#import "LatestWeatherViewController.h"
#import "WeatherDetailsController.h"
#import "LatestActivityCollectionCell.h"
#import "LatestListingCollectionCell.h"
#import "NSDate+Helper.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "RegistrationViewController.h"
#import "SlideNavigationController.h"
#import "Events.h"
#import "Listing.h"
#import "Weather.h"
#import "HomeWeatherTableViewCell.h"
#import "HomeLatestListingTableViewCell.h"
#import "HomeLatestEventsTableViewCell.h"
#import "FUICustomAuthDelegate.h"

#import <FirebaseAuthUI/FirebaseAuthUI.h>
#import <FirebaseFacebookAuthUI/FUIFacebookAuth.h>
#import <FirebaseGoogleAuthUI/FUIGoogleAuth.h>
#import <FirebaseTwitterAuthUI/FUITwitterAuth.h>
#import "FUICustomAuthPickerViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>



@import GoogleMobileAds;
@import Firebase;
@interface HomeViewController () <UITableViewDelegate,
                                  UITableViewDataSource,
                                  GADBannerViewDelegate,
                                  NetworkLayerDelegate,
                                  FUIAuthDelegate,
                                  HomeLatestListingTableViewCellDelegate>
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet GADBannerView *googleBannerVIew;
@property (nonatomic, strong) LoginViewController *loginView;
@property (nonatomic, strong) WeatherDetailsController *weatherDetails;
@property (nonatomic,strong) RegistrationViewController *registrationController;
@property (nonatomic,strong) NSMutableDictionary *weatherData;
@property (nonatomic, strong) NSMutableArray *listingData;
@property (nonatomic, strong) NSMutableArray *eventsData;
@property (nonatomic, strong) Weather *weatherModel;
@property (nonatomic,strong) EventDetailsViewController *eventDetails;
@property (nonatomic, strong) ListingDetailsViewController *listingsDetails;
@property (nonatomic) FIRAuth *auth;
@property (nonatomic) FUIAuth *authUI;
@property (nonatomic) id<FUIAuthDelegate> customAuthUIDelegate;
@property (nonatomic) FIRAuthStateDidChangeListenerHandle authStateDidChangeHandle;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBannerOnThisView];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
//    [self.tableView registerClass:[HomeWeatherTableViewCell class] forCellReuseIdentifier:@"HomeWeatherCell"];
//    [self.tableView registerClass:[HomeLatestListingTableViewCell class] forCellReuseIdentifier:@"HomeLatestListingCell"];
//    [self.tableView registerClass:[HomeLatestEventsTableViewCell class] forCellReuseIdentifier:@"HomeLatestEventsCell"];
    
    _tableView.estimatedRowHeight = 400.0;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.customAuthUIDelegate = [[FUICustomAuthDelegate alloc] init];
    
    self.auth = [FIRAuth auth];
    self.authUI = [FUIAuth defaultAuthUI];
    //set AuthUI Delegate

    
    self.authUI.delegate = self.customAuthUIDelegate;
   
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.auth.currentUser) {
        
        _authUI.providers = [[Global sharedInstance] getFireBaseProviders];
        FUICustomAuthPickerViewController *customPicker = (FUICustomAuthPickerViewController *)[self.customAuthUIDelegate authPickerViewControllerForAuthUI:self.authUI];
        customPicker.btnClose.hidden = true;
        UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:customPicker];
        controller.navigationBar.hidden = YES;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        [APP_DELEGATE getAppConfig];
        [self getDataFromApi];
    }    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)refreshData:(UIRefreshControl *)refreshControl
{
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSThread sleepForTimeInterval:3];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM d, h:mm a"];
            NSString *lastUpdate = [NSString stringWithFormat:@"Last updated on %@", [formatter stringFromDate:[NSDate date]]];
            
            refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdate];
            
            [self getDataFromApi];
            
            [refreshControl endRefreshing];
            
            //NSLog(@"refresh end");
        });
    });
}

-(void) addBannerOnThisView {
    self.googleBannerVIew.adUnitID = kGOOGLE_BANNER_ADD_UNIT;
    self.googleBannerVIew.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    self.googleBannerVIew.delegate = self;
    // Enable test ads on simulators.
    request.testDevices = @[@"d1c340abb06de75c6a29e08865770c8be343e606",
                            @"c1589702d7b51e99ba56a98c3ccad9b85f75109f",
                            @"59daea62306d7c61bbb77cb637e1e389b21ee23c"];
    [self.googleBannerVIew loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 Pragma: Mark : UITableViewDelegate
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 250;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        if(indexPath.row == 0) {
            HomeWeatherTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HomeWeatherCell"];
            [cell populateWeather:self.weatherModel];
            //[cell retrieveWeatherData];
            return cell;
        }
        
        
    } else if (indexPath.section == 1) {
        if(indexPath.row == 0) {
            HomeLatestListingTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HomeLatestListingCell"];
            [cell populateListingData: self.listingData];
            //[cell retrieveListingData];
            cell.delegate = self;
            return cell;
        }
    } else if (indexPath.section == 2) {
        
        if (indexPath.row == 0) {
            
            HomeLatestEventsTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"HomeLatestEventsCell"];
            [cell populateEventsData:self.eventsData];
            //[cell retrieveEventsData];
            return cell;
        }
        
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   if (indexPath.section == 0) {
       if (indexPath.row == 0) {
           [self didTapShowWeatherDetail];
       }
   }
}

#pragma mark API Calls

-(void) getDataFromApi {
    //Weather
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"weather"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        self.weatherModel = [Weather weatherWithObject:snapshot.value];
        //[self populateWeather:self.weatherModel];
        HomeWeatherTableViewCell *cell = (HomeWeatherTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        [cell populateWeather:self.weatherModel];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation: UITableViewRowAnimationNone];
        [self.tableView reloadData];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
    
    
    //Events
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"events"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *eventsArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Events *model = [Events eventWithObject:children.value];
            [eventsArray addObject:model];
        }
        
        [self.eventsData addObjectsFromArray:eventsArray];
        
        HomeLatestEventsTableViewCell *cell = (HomeLatestEventsTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        [cell populateEventsData:eventsArray];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:2];
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation: UITableViewRowAnimationNone];
        [self.tableView reloadData];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
    
    //Listings
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"listing"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *listingArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Listing *model = [Listing listingWithObject:children.value];
            [listingArray addObject:model];
        }
        
        [self.listingData addObjectsFromArray:listingArray];
        
        HomeLatestListingTableViewCell *cell = (HomeLatestListingTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        
        [cell populateListingData: listingArray];
        
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:1];
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexpath, nil] withRowAnimation: UITableViewRowAnimationNone];
        [self.tableView reloadData];
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch listings list" message:[error localizedDescription]];
    }];
    
     [self.tableView reloadData];
}

#pragma mark - latestEvents Delegate

-(void) didTapShowEventDetail:(Events *)data {
    if (!self.eventDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.eventDetails = [storyboard instantiateViewControllerWithIdentifier:@"EventDetailsController"];
    }
    
    self.eventDetails.event = data;
    
    self.navigationController.title = @"";
    [self.navigationController pushViewController:self.eventDetails animated:YES];
    
}

#pragma mark - latestlistings Delegate

-(void) didTapShowListingDetail:(Listing *)data {
    if (!self.listingsDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.listingsDetails = [storyboard instantiateViewControllerWithIdentifier:@"ListingDetails"];
    }
    
    self.listingsDetails.listing = data;
    
    [self.navigationController pushViewController:self.listingsDetails animated:YES];
}

#pragma mark - WeatherDetails Delegate

-(void) didTapShowWeatherDetail {
    if (!self.weatherDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Weather" bundle:nil];
        self.weatherDetails = [storyboard instantiateViewControllerWithIdentifier:@"WeatherDetails"];
    }
    
    [GlobalInstance setViewFromDashboard:NO];
    
    self.weatherDetails.weatherObject = self.weatherModel;
    
    [self.navigationController pushViewController:self.weatherDetails animated:YES];
}

#pragma mark - HomeLatestListingTableViewCell Delegate

-(void) didSelectListing:(Listing *)listing {
    [self didTapShowListingDetail:listing];
}

#pragma mark - FUIAuthDelegate methods

// this method is called only when FUIAuthViewController is delgate of AuthUI
- (void)authUI:(FUIAuth *)authUI didSignInWithUser:(nullable FIRUser *)user error:(nullable NSError *)error {
    if (error) {
        if (error.code == FUIAuthErrorCodeUserCancelledSignIn) {
            
        } else {
            
        }
    }
}


@end
