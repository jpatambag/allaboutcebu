//
//  LatestViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "LatestViewController.h"
#import "Global.h"
@import Firebase;

@interface LatestViewController ()<UIWebViewDelegate>
@property (nonatomic, weak) IBOutlet UIWebView *webView;
//@property (nonatomic, strong) FIRDatabaseReference *ref;
@end

@implementation LatestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.ref = [[FIRDatabase database] reference];
    //[self.ref keepSynced:YES];
    [self getData];
    // Do any additional setup after loading the view.
}

-(void) getData {
    /*[[[self.ref child:@"cebu"] child:@"latest"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        //NSLog(@"response object :%@",[snapshot.value objectForKey:@"url"]);
        NSString *urlStr = [snapshot.value objectForKey:@"url"];
        NSURL *latest = [NSURL URLWithString:urlStr];
        [self.webView loadRequest:[NSURLRequest requestWithURL:latest]];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];*/
    
    NSString *sourceURL = [Global sharedInstance].latestEndpointURL;
    
    if (![sourceURL isEqualToString:@""]) {
        NSURL *latest = [NSURL URLWithString:sourceURL];
        [self.webView loadRequest:[NSURLRequest requestWithURL:latest]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
