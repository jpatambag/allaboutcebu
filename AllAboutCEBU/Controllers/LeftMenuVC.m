
#import "LeftMenuVC.h"
#import "Global.h"
#import "LeftMenuCell.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "WeatherDetailsController.h"
#import "ListingViewController.h"
#import "RootViewController.h"
#import "RegistrationViewController.h"
#import "AboutViewController.h"
#import "ListCategoryViewController.h"
#import "CategoriesViewController.h"

@interface LeftMenuVC () <UITableViewDataSource, UITableViewDelegate> {
    NSInteger selectedIndex;
}
@property (nonatomic) IBOutlet UITableView *tblMenu;
@property (nonatomic,strong) NSMutableArray *menuArray;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;
@property (nonatomic,strong) IBOutlet UIImageView *profilePic;
@property (nonatomic,strong) IBOutlet UILabel *fullname;
@end

@implementation LeftMenuVC

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = YES;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //selected dinner track from meal section
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfile:) name:@"updateProfile" object:nil];
    
    self.menuArray = [[NSMutableArray alloc] initWithArray:[[Global sharedInstance] loadPlistFile:@"MenuControllers"]];
    [self.tblMenu reloadData];
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self updateContentsOnLeftMenu:nil];
}

-(void)updateContentsOnLeftMenu:(NSDictionary *)fbData {
    
    NSDictionary *fbDataObject = [[NSUserDefaults standardUserDefaults] objectForKey:kFBDATA];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/picture?",kFBGET_PROFILE_PIC,[fbDataObject objectForKey:@"FbID"]]];
    
    self.profilePic.contentMode = UIViewContentModeScaleAspectFit;
    [self.profilePic setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    self.fullname.text = [NSString stringWithFormat:@"%@ %@",[fbDataObject objectForKey:@"FirstName"],[fbDataObject objectForKey:@"Surname"]];
    
    //NSLog(@"DATA ON LEFT MENU :%@",fbData);
    
}

-(void) updateProfile:(NSNotification *)notification {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.menuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuCell"];
    [cell bindDataonCell:[self.menuArray objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIViewController *controller;
    NSString *controllerStr = [[self.menuArray objectAtIndex:indexPath.row] objectForKey:@"controller"];
    controller = [self.storyboard instantiateViewControllerWithIdentifier:controllerStr];

    [self.tblMenu deselectRowAtIndexPath:[self.tblMenu indexPathForSelectedRow] animated:YES];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:controller
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
    
    if (indexPath.row == selectedIndex) {
        //currently active, no need to send message to applewatch...
    
    }else {
        [self sendMessageToChangeRootController:indexPath.row];
    }
    selectedIndex = indexPath.row;
}

- (void)sendMessageToChangeRootController:(NSInteger)row {
    NSLog(@"selected controller");
} */

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuCell"];
    cell.textLabel.textColor = [UIColor blackColor];
    switch (indexPath.row)
    {
        case 0:
            cell.textLabel.text = @"Dashboard";
            break;
            
        case 1:
            cell.textLabel.text = @"Weather";
            break;
            
        case 2:
            cell.textLabel.text = @"Places";
            break;
            
        case 3:
            cell.textLabel.text = @"About Us";
            break;
    }
    cell.textLabel.font = [UIFont fontWithName:@"Roboto" size:14.0];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *vc ;
    
    switch (indexPath.row)
    {
        case 0:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"RootController"];
            break;
            
        case 1:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"WeatherDetails"];
            [GlobalInstance setViewFromDashboard:YES];
            break;
            
        case 2:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"CategoriesViewController"];
            [GlobalInstance setViewFromDashboard:YES];
            break;
            
        case 3:
            [self.tblMenu deselectRowAtIndexPath:[self.tblMenu indexPathForSelectedRow] animated:YES];

            //[self signOut];
            //[[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            //[[NSNotificationCenter defaultCenter] postNotificationName:kSHOWREGISTRATION_FORM object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"AboutController"];
            [GlobalInstance setViewFromDashboard:NO];
            
            break;

    }
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:nil];
}

-(void) signOut {
}


- (void)changeSelectedIndex {
    selectedIndex = -1;
}

@end
