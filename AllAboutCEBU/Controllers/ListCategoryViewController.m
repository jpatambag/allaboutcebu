//
//  ListCategoryViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/8/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "ListCategoryViewController.h"
#import "CategoryCell.h"
#import "NetworkLayer.h"
#import "Constants.h"
#import "ListingViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "LoadMoreTableFooterView.h"
#import "SearchPlaceController.h"

@interface ListCategoryViewController () <UITableViewDataSource, UITableViewDelegate, NetworkLayerDelegate, EGORefreshTableHeaderDelegate, LoadMoreTableFooterDelegate> {
    
    NetworkLayer *network;
    
    EGORefreshTableHeaderView *egoRefreshTableHeaderView;
    BOOL isRefreshing;
    
    LoadMoreTableFooterView *loadMoreTableFooterView;
    BOOL isLoadMoreing;
    
    int dataRows;
    
    int page;
}

@property (nonatomic,strong) IBOutlet UITableView *tblist;
@property (nonatomic,strong) NSMutableArray *tableData;
@property (nonatomic,strong) ListingViewController *listingView;
@property (nonatomic,strong) SearchPlaceController *searchController;
@end


@implementation ListCategoryViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.tableData) {
        self.tableData = [[NSMutableArray alloc] init];
    }
    
    page = 1;
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
    
    [self getDataFromAPIWithPageNo:page];
    
    // Do any additional setup after loading the view.
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) getDataFromAPIWithPageNo:(int) index {
    [network requestDataFromServer:kGET_LIST_OF_CATEGORY andParams:nil forTag:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark: NetworkDelegate Methods

-(void)serverResponse:(id)response tag:(int)apiTag {
    //[self.tableData removeAllObjects];
    
    page += 1;
    
    [self.tableData addObjectsFromArray:(NSArray *)response];
    
    [self.tblist reloadData];
}


#pragma mark: UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return[self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"CategoryCell";
    CategoryCell *cell = (CategoryCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //[cell setBackgroundColor:[UIColor clearColor]];
    //[cell populateDataOnCell:[self.tableData objectAtIndex:indexPath.row]];
    [cell bindDataOnCell:[self.tableData objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!self.searchController) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Places" bundle:nil];
        self.searchController = [storyboard instantiateViewControllerWithIdentifier:@"SearchPlaceController"];
    }
    
    
    NSLog(@"OBject selected :%@",[self.tableData objectAtIndex:indexPath.row]);
    
    self.searchController.placesObject = nil;
    self.searchController.placesObject = [self.tableData objectAtIndex:indexPath.row];
    //self.listingDetails.dataDict = [self.tableData objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:self.searchController animated:YES];
    
}

- (void)reloadData
{
    [self.tblist reloadData];
    
    loadMoreTableFooterView.frame = CGRectMake(0.0f, self.tblist.contentSize.height, self.view.frame.size.width, self.tblist.bounds.size.height);
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidScroll:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidEndDragging:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    isRefreshing = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        //dataRows = 20; // show first 20 records after refreshing
        [self getDataFromAPIWithPageNo:1];
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete refreshing
            isRefreshing = NO;
            [self reloadData];
            
            [egoRefreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tblist];
        });
    });
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return isRefreshing;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

#pragma mark LoadMoreTableFooterDelegate Methods

- (void)loadMoreTableFooterDidTriggerLoadMore:(LoadMoreTableFooterView*)view
{
    isLoadMoreing = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        [self getDataFromAPIWithPageNo:page];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete loading...
            isLoadMoreing = NO;
            
            [self reloadData];
            
            [loadMoreTableFooterView loadMoreScrollViewDataSourceDidFinishedLoading:self.tblist];
        });
    });
}
- (BOOL)loadMoreTableFooterDataSourceIsLoading:(LoadMoreTableFooterView*)view
{
    return isLoadMoreing;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
@end
