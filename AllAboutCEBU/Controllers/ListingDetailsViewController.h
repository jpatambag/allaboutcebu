//
//  ListingDetailsViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Listing.h"
@class GADBannerView;
@interface ListingDetailsViewController : UIViewController
@property (nonatomic, strong) Listing *listing;
@end
