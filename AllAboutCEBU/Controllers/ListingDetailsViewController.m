//
//  ListingDetailsViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

@import GoogleMobileAds;

#import "ListingDetailsViewController.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Global.h"
#import <MapKit/MapKit.h>
#define METERS_PER_MILE 1609.344

@interface ListingDetailsViewController () <GADBannerViewDelegate, MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblVenue;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
@property (strong, nonatomic) IBOutlet UIButton *btnNumber;
@property (strong, nonatomic) IBOutlet UITextView *txtDesc;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet GADBannerView *googleBannerVIew;
-(IBAction)onBtnTapCallNumber:(UIButton *)sender;
@end

@implementation ListingDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    //self.scrollView.frame = CGRectMake(0, self.scrollView.bounds.size.height - self.navigationController.navigationBar.bounds.size.height - 100, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height);
    
    //[[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    
    [self addBannerOnThisView];
}

-(void) viewWillAppear:(BOOL) animated {
    [super viewWillAppear:animated];
}

-(void) addBannerOnThisView {
    self.googleBannerVIew.adUnitID = kGOOGLE_BANNER_ADD_UNIT;
    self.googleBannerVIew.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    self.googleBannerVIew.delegate = self;
    // Enable test ads on simulators.
    request.testDevices = @[@"d1c340abb06de75c6a29e08865770c8be343e606",@"c1589702d7b51e99ba56a98c3ccad9b85f75109f",@"59daea62306d7c61bbb77cb637e1e389b21ee23c"];
    [self.googleBannerVIew loadRequest:request];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //self.imgItem.image = [UIImage imageNamed:@"placeholder.png"];
    
    [self populateDataOnView];
    //[self adjustScrollViewContentSize];
    [self showMapWithPins];
}

-(void) showMapWithPins {
    //self.mapView.frame = self.view.bounds;
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [_listing.lat floatValue];
    zoomLocation.longitude= [_listing.lng floatValue];
    
    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
    myAnnotation.coordinate = zoomLocation;
    myAnnotation.title = _listing.name;
    myAnnotation.subtitle = _listing.address;
    [self.mapView addAnnotation:myAnnotation];
    
    // 2
    //MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5* METERS_PER_MILE);
    
    // 3
    //[_mapView setRegion:viewRegion animated:YES];
    
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (zoomLocation, 2000, 2000);
//    [_mapView setRegion:region animated:YES];
    
    MKCoordinateRegion region;
    region.center.latitude = [_listing.lat floatValue];
    region.center.longitude =[_listing.lng floatValue];
    //region.span.latitudeDelta = 0.112872;
    //region.span.longitudeDelta = 0.109863;
    [self.mapView setRegion:region animated:YES];

}

-(void)adjustScrollViewContentSize {
    
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 314);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 218);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 818);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 395);
    }
}

-(void) populateDataOnView {
    
    /*[self.imgItem setImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSDictionary *filter = [self.dataDict dictionaryByReplacingNullsWithBlanks];

    self.title = [filter objectForKey:@"Name"];
    
    self.lblTitle.text = [filter objectForKey:@"Name"];
    
//    NSURL *phoneNumber = [NSURL URLWithString:@"telprompt://13232222222"];
//    [[UIApplication sharedApplication] openURL:phoneNumber];
    
    //self.lblDateTime.text = [filter objectForKey:@"PhoneNumber"];
    
    [self.btnNumber setTitle:[filter objectForKey:@"PhoneNumber"] forState:UIControlStateNormal];
    
    //self.lblDateTime.userInteractionEnabled = YES;
    //[self.lblDateTime addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    self.lblVenue.text = [filter objectForKey:@"Address"];
    self.txtDesc.text = [filter objectForKey:@"Description"];*/
    
    NSString *imageURL = _listing.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.imgItem.image = image;
                                     
                                     NSLog(@"loaded latestListings images");
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];
    
    
    self.lblTitle.text = _listing.name;
    self.lblVenue.text = _listing.address;
    
}

-(IBAction)onBtnTapCallNumber:(UIButton *)sender {
    NSLog(@"call number");
    NSString *phNo = sender.titleLabel.text;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Call facility not available or Invalid number provided" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [calert show];
    }

}

#pragma mark Delegate Methods

/*-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    id <MKAnnotation> annotation = [view annotation];
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        NSLog(@"Clicked Pizza Shop");
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Disclosure Pressed" message:@"Click Cancel to Go Back" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alertView show];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.canShowCallout = YES;
            pinView.image = [UIImage imageNamed:@"pizza_slice_32.png"];
            pinView.calloutOffset = CGPointMake(0, 32);
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
            
            // Add an image to the left callout.
            UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pizza_slice_32.png"]];
            pinView.leftCalloutAccessoryView = iconView;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}*/

#pragma mark - ADBanner Delegate

- (void) adView: (GADBannerView*) view didFailToReceiveAdWithError: (GADRequestError*) error {
    
}

- (void) adViewDidReceiveAd: (GADBannerView*) view {
    
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
