//
//  ListingViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "ListingViewController.h"
#import "ListingCell.h"
#import "NetworkLayer.h"
#import "Constants.h"
#import "ListingDetailsViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "LoadMoreTableFooterView.h"
#import "SlideNavigationController.h"

@interface ListingViewController () <UITableViewDataSource, UITableViewDelegate, NetworkLayerDelegate, EGORefreshTableHeaderDelegate, LoadMoreTableFooterDelegate,SlideNavigationControllerDelegate> {
    NetworkLayer *network;
    
    EGORefreshTableHeaderView *egoRefreshTableHeaderView;
    BOOL isRefreshing;
    
    LoadMoreTableFooterView *loadMoreTableFooterView;
    BOOL isLoadMoreing;
    
    int dataRows;
    
    int page;
}
@property (nonatomic,strong) IBOutlet UITableView *tblist;
@property (nonatomic,strong) NSMutableArray *tableData;
@property (nonatomic,strong) ListingDetailsViewController *listingDetails;
@end

@implementation ListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.tableData) {
        self.tableData = [[NSMutableArray alloc] init];
    }
    
    page = 1;
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
    
    [self getDataFromAPIWithPageNo:page];
    
    // Do any additional setup after loading the view.
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) getDataFromAPIWithPageNo:(int) index {
    [network requestDataFromServer:kLISTING andParams:nil forTag:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark: NetworkDelegate Methods

-(void)serverResponse:(id)response tag:(int)apiTag {
    //[self.tableData removeAllObjects];
    
    page += 1;
    
    [self.tableData addObjectsFromArray:(NSArray *)response];
    
    [self.tblist reloadData];
}


#pragma mark: UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return[self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ListingCell";
    ListingCell *cell = (ListingCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell populateDataOnCell:[self.tableData objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (!self.listingDetails) {
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     self.listingDetails = [storyboard instantiateViewControllerWithIdentifier:@"ListingDetails"];
 }
 
 //self.listingDetails.dataDict = [self.tableData objectAtIndex:indexPath.row];
 
 [self.navigationController pushViewController:self.listingDetails animated:YES];

}

- (void)reloadData
{
    [self.tblist reloadData];
    
    loadMoreTableFooterView.frame = CGRectMake(0.0f, self.tblist.contentSize.height, self.view.frame.size.width, self.tblist.bounds.size.height);
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidScroll:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [egoRefreshTableHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    [loadMoreTableFooterView loadMoreScrollViewDidEndDragging:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    isRefreshing = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        //dataRows = 20; // show first 20 records after refreshing
        [self getDataFromAPIWithPageNo:1];
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete refreshing
            isRefreshing = NO;
            [self reloadData];
            
            [egoRefreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tblist];
        });
    });
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return isRefreshing;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

#pragma mark LoadMoreTableFooterDelegate Methods

- (void)loadMoreTableFooterDidTriggerLoadMore:(LoadMoreTableFooterView*)view
{
    isLoadMoreing = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // waiting for loading data from internet
        sleep(3);
        [self getDataFromAPIWithPageNo:page];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            // complete loading...
            isLoadMoreing = NO;
            
            [self reloadData];
            
            [loadMoreTableFooterView loadMoreScrollViewDataSourceDidFinishedLoading:self.tblist];
        });
    });
}
- (BOOL)loadMoreTableFooterDataSourceIsLoading:(LoadMoreTableFooterView*)view
{
    return isLoadMoreing;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

@end
