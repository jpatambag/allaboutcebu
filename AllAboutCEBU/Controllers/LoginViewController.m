//
//  LoginViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 31/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "Global.h"
#import "Constants.h"

@interface LoginViewController () {
    
}
@property (nonatomic, strong) RegistrationViewController *registrationView;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstrain;
@property (nonatomic,weak) IBOutlet UITextView *txtDesc;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self resetAutoLayoutConstraints];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addContentsOnUITextView];
    [self adjustScrollViewContentSize];
}

-(void)addContentsOnUITextView {
    [self.txtDesc setContentOffset:CGPointZero animated:YES];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, [UIScreen mainScreen].bounds.size.width, 30)];
    
    lblTitle.font = [UIFont fontWithName:@"Roboto" size:15];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = @"AllAboutCEBU End User License Agreement?";
    [self.txtDesc addSubview:lblTitle];
    
    CGSize textViewSize = [self findHeightForText:self.txtDesc.text havingWidth:[UIScreen mainScreen].bounds.size.width andFont:[UIFont fontWithName:@"Roboto" size:12]];
    
    UIButton *myButton  =   [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame      =   CGRectMake(0,textViewSize.height - 50, [UIScreen mainScreen].bounds.size.width, 50.0);
    [myButton setTitle:@"ACCEPT" forState:UIControlStateNormal];
    myButton.titleLabel.textColor = [UIColor blackColor];
    [myButton addTarget:self action:@selector(onBtnAccept:) forControlEvents:UIControlEventTouchUpInside];
    [self.txtDesc addSubview:myButton];
    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, myButton.bounds.origin.y + 50, [UIScreen mainScreen].bounds.size.width, 50)];
//    paddingView.backgroundColor = [UIColor redColor];
//    
//    [self.txtDesc addSubview:paddingView];
    
    
}

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        //iOS 7
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

-(void)adjustScrollViewContentSize {
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 314);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 218);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 180);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 395);
    }
}

-(void)resetAutoLayoutConstraints {
    CGSize sizeThatFitsTextView = [self.txtDesc sizeThatFits:CGSizeMake(self.txtDesc.frame.size.width, MAXFLOAT)];
    
    self.textViewHeightConstrain.constant = sizeThatFitsTextView.height;
    self.txtDesc.frame = CGRectMake(13, 342, 379, sizeThatFitsTextView.height);

}

-(IBAction)onBtnAccept:(UIButton *)sender {
    
    
    //if (!self.registrationView) {
        self.registrationView = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
    //}
    
    [self.navigationController pushViewController:self.registrationView animated:YES];
}

@end
