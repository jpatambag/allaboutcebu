//
//  MainViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 27/3/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "MainViewController.h"
#import "RootViewController.h"
#import "WeatherDetailsController.h"
#import "CategoriesViewController.h"
#import "AboutViewController.h"
#import "LatestViewController.h"
#import "AllAboutCEBU-Swift.h"
#import "HomeViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpViewControllers];
    // Do any additional setup after loading the view.
}

-(void) setUpViewControllers {
    UIStoryboard *homeStoryBoard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIStoryboard *weatherStoryBoard = [UIStoryboard storyboardWithName:@"Weather" bundle:nil];
    UIStoryboard *placesStoryBoard = [UIStoryboard storyboardWithName:@"Places" bundle:nil];
    
    UIImage *dashboardIcon = [UIImage imageNamed:@"home_default"];
    UIImage *weatherIcon = [UIImage imageNamed:@"weather_default"];
    UIImage *placesIcon = [UIImage imageNamed:@"places_default"];
    UIImage *aboutIcon = [UIImage imageNamed:@"about_default"];
    
//    RootViewController *rootVc = [mainStoryBoard instantiateViewControllerWithIdentifier:@"RootController"];
//    rootVc.title = @"Home";
//    rootVc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Home" image:dashboardIcon tag:0];
    
    
    
    HomeViewController *rootVc = [homeStoryBoard instantiateViewControllerWithIdentifier:@"HomeController"];
    rootVc.title = @"Home";
    rootVc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Home" image:dashboardIcon tag:0];
    
    LatestViewController *latestController = [weatherStoryBoard instantiateViewControllerWithIdentifier:@"LatestController"];
    latestController.title = @"Latest";
    latestController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Latest" image:weatherIcon tag:1];
    
    CategoriesViewController *categories = [placesStoryBoard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
    categories.title = @"Places";
    categories.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Places" image:placesIcon tag:2];
    
    AboutViewController *about = [mainStoryBoard instantiateViewControllerWithIdentifier:@"AboutController"];
    about.title = @"About";
    about.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"About" image:aboutIcon tag:3];

    //AuthViewController *about = [mainStoryBoard instantiateViewControllerWithIdentifier:@"AuthViewController"];
    //about.title = @"About";
    //about.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"About" image:aboutIcon tag:3];
    
    
    
    UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:rootVc];
    
    UINavigationController *weatherNav = [[UINavigationController alloc] initWithRootViewController:latestController];
    
    UINavigationController *categoriesNav = [[UINavigationController alloc] initWithRootViewController:categories];
    
    UINavigationController *aboutNav = [[UINavigationController alloc] initWithRootViewController:about];
    [aboutNav setNavigationBarHidden:YES];
    
    self.viewControllers = [NSArray arrayWithObjects:mainNav,weatherNav, categoriesNav, aboutNav, nil];

    self.selectedIndex = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
