//
//  RegistrationViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 7/11/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "RegistrationViewController.h"
//#import "FacebookObject.h"
#import "Constants.h"
#import "Global.h"
//#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "NetworkLayer.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"

//@interface RegistrationViewController () <FacebookObjectDelegate, NetworkLayerDelegate> {
@interface RegistrationViewController () <NetworkLayerDelegate> {
    NetworkLayer *network;
}
//@property (nonatomic, strong) FacebookObject *facebookObject;
@end


@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"VIEW DID APPEAR ON REGISTRTION");
    
    [self performSelector:@selector(hideRegistrationView) withObject:nil afterDelay:3.0];
    
    //NSLog(@"NSUserDefaults dump: %@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
}

-(void) hideRegistrationView {
    [[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:nil];
}

//-(IBAction)onBtnRegisterOnFacebook:(UIButton *)sender {
//    self.facebookObject = [[FacebookObject alloc] init];
//    self.facebookObject.delegate = self;
//    
//    // If the session state is any of the two "open" states when the button is clicked
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        // Close the session and remove the access token from the cache
//        // The session state handler (in the app delegate) will be called automatically
//        [FBSession.activeSession closeAndClearTokenInformation];
//        
//        // If the session state is not any of the two "open" states when the button is clicked
//    } else {
//        // Open a session showing the user the login UI
//        // You must ALWAYS ask for basic_info permissions when opening a session
//        [FBSession openActiveSessionWithReadPermissions:@[@"user_birthday,email"]
//                                           allowLoginUI:YES
//                                      completionHandler:
//         ^(FBSession *session, FBSessionState state, NSError *error) {
//             
//             // Retrieve the app delegate
//             //AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
//             //[appDelegate sessionStateChanged:session state:state error:error];
//            
//             
//             
//             [self.facebookObject sessionStateChanged:session state:state error:error];
//         }];
//    }
//}

-(IBAction)onBtnRegisterOnFacebook:(UIButton *)sender {
    //self.facebookObject = [[FacebookObject alloc] init];
    //self.facebookObject.delegate = self;
    
    /*// If the session state is any of the two "open" states when the button is clicked
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for basic_info permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"user_birthday,email"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             // Retrieve the app delegate
             //AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
             // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
             //[appDelegate sessionStateChanged:session state:state error:error];
             
             
             
             [self.facebookObject sessionStateChanged:session state:state error:error];
         }];
    } */

     //[self.facebookObject loginToFacebook:nil];
//    if ([FBSDKAccessToken currentAccessToken]) {
//         [self.facebookObject loginToFacebook:nil];
//    } else {
//        [self.facebookObject loginToFacebook:nil];
//    }
}


/*-(void) didLoginWithFacebook:(NSString *)token {
    
    
    //[SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
    NSDictionary *tokenDictParam = @{@"facebook_access_token" : [FBSDKAccessToken currentAccessToken].tokenString};
    
    NSDictionary *dRequestParam = @{ @"user": tokenDictParam };
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[tokenDictParam objectForKey:@"facebook_access_token"] forKey:kFACEBOOK_TOKEN];
    
    [defaults setObject:kREGISTERED forKey:@"1"];
    
    
    if ([defaults synchronize]) {
        
        [self requestUserInfo];
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:nil];
    }
    
    
    
}*/

-(void)didLoginTofacebook:(NSMutableDictionary *)object {
    
    /*if ([[FBSDKAccessToken currentAccessToken].permissions containsObject:@"publish_actions"]) {
        [self.facebookObject getFacebookUserInformation];
    } else {
        //[self.facebookObject requestPublishActionsPermission];
        [self.facebookObject getFacebookUserInformation];
    }*/
    
}

-(void)didGetFacebookUserInformation:(NSMutableDictionary *)object {
    
    
    if ([object objectForKey:@"userFacebookData"]) {
        
        NSDictionary *result = [object objectForKey:@"userFacebookData"];
        
        NSString *dataRequest = [NSString stringWithFormat:@"%@%@",kCHECK_USER_EXIST,[result objectForKey:@"email"]];
        
        //[network requestDataFromServer:dataRequest andParams:nil forTag:0];
        [network requestDataServerWithDataObject:dataRequest andParams:nil forTag:0 withObject:result];
    }
}

/*- (void)requestUserInfo {
    // We will request the user's public picture and the user's birthday
    // These are the permissions we need:
    
    NSArray *permissionsNeeded = @[@"user_birthday,email"];
    
    // Request the permissions the user currently has
    [FBRequestConnection startWithGraphPath:@"/me/permissions"
      completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
          if (!error){
              // These are the current permissions the user has
              NSDictionary *currentPermissions= [(NSArray *)[result data] objectAtIndex:0];
              
              // We will store here the missing permissions that we will have to request
              NSMutableArray *requestPermissions = [[NSMutableArray alloc] initWithArray:@[]];
              
              // Check if all the permissions we need are present in the user's current permissions
              // If they are not present add them to the permissions to be requested
              for (NSString *permission in permissionsNeeded){
                  if (![currentPermissions objectForKey:permission]){
                      [requestPermissions addObject:permission];
                  }
              }
              
              // If we have permissions to request
              if ([requestPermissions count] > 0){
                  // Ask for the missing permissions
//                  [FBSession.activeSession
//                   requestNewReadPermissions:requestPermissions
//                   completionHandler:^(FBSession *session, NSError *error) {
//                       if (!error) {
//                           // Permission granted, we can request the user information
//                           [self makeRequestForUserData];
//                       } else {
//                           // An error occurred, we need to handle the error
//                           // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
//                           NSLog(@"error %@", error.description);
//                       }
//                   }];
                   [self makeRequestForUserData];
              } else {
                  // Permissions are present
                  // We can request the user information
                  [self makeRequestForUserData];
              }
              
          } else {
              // An error occurred, we need to handle the error
              // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
              NSLog(@"error %@", error.description);
          }
      }];
}

- (void) makeRequestForUserData
{
    
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Success! Include your code to handle the results here
            NSLog(@"user info: %@", result);
            self.facebookObject.fbResponseObject = result;
            
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfileName" object:self.facebookObject.fbResponseObject];
            

            NSString *dataRequest = [NSString stringWithFormat:@"%@%@",kCHECK_USER_EXIST,[result objectForKey:@"email"]];
            
            [network requestDataFromServer:dataRequest andParams:nil forTag:0];

            
        } else {
            // An error occurred, we need to handle the error
            // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
            NSLog(@"error %@", error.description);
        }
    }];

} */

#pragma mark: NetworkDelegate Methods

-(void) serverResponseWithDataObject:(id) response tag:(int)apiTag object:(id)theObject {
    if (apiTag == 0) {
        
        NSArray *arrayResponse = (NSArray *)response;
        
        if ([arrayResponse count] == 0) {
            
            /*NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"%@",[theObject objectForKey:@"id"]],@"FbID",
                                    [theObject objectForKey:@"birthday"],@"BirthDate",
                                    [NSString stringWithFormat:@"%@",[theObject objectForKey:@"first_name"]],@"FirstName",
                                    [NSString stringWithFormat:@"%@",[theObject objectForKey:@"last_name"]],@"Surname",
                                    [theObject objectForKey:@"email"],@"Email",
                                    nil];*/

            
            [self makeApiRequestForFBSignup:theObject];
        } else {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[[arrayResponse objectAtIndex:0] dictionaryByReplacingNullsWithBlanks] forKey:kFBDATA];
            
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:[[arrayResponse objectAtIndex:0] dictionaryByReplacingNullsWithBlanks]];
            
        }
    }
}

-(void)serverResponse:(id)response tag:(int)apiTag{
    //NSLog(@"response for check :%@",response);
    //NSLog(@"facebook response :%@",self.facebookObject.fbResponseObject);
    
     if (apiTag == 1) {
         
         if ([response isKindOfClass:[NSArray class]]) {
             NSArray *arrayResponse = (NSArray *)response;
             
             [[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:[[arrayResponse objectAtIndex:0] dictionaryByReplacingNullsWithBlanks]];
         } else if ([response isKindOfClass:[NSDictionary class]])  {
             [[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:[response dictionaryByReplacingNullsWithBlanks]];
         }
        
        
    }

}

-(void) makeApiRequestForFBSignup:(NSDictionary *)result {
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSString stringWithFormat:@"%@",[result objectForKey:@"id"]],@"FbID",
                            @"",@"BirthDate",
                            [NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]],@"FirstName",
                            [NSString stringWithFormat:@"%@",[result objectForKey:@"last_name"]],@"Surname",
                            [result objectForKey:@"email"],@"Email",
                            nil];
    
    [self requestData:params];

    /*[FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Success! Include your code to handle the results here
            //NSLog(@"user info: %@", result);
            self.facebookObject.fbResponseObject = result;
            
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"%@",[result objectForKey:@"id"]],@"FbID",
                                    [result objectForKey:@"birthday"],@"BirthDate",
                                    [NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]],@"FirstName",
                                    [NSString stringWithFormat:@"%@",[result objectForKey:@"last_name"]],@"Surname",
                                    [result objectForKey:@"email"],@"Email",
                                    nil];
            
            [self requestData:params];
            
        } else {
            // An error occurred, we need to handle the error
            // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
            NSLog(@"error %@", error.description);
        }
    }]; */

}

-(void) requestData:(NSDictionary *)params {
    
    NSDictionary *final_params  = [NSDictionary dictionaryWithObject:params forKey:@"user"];
    
    [network requestPOSTDataToServer:kGET_USER_INFO andParams:final_params forTag:1];
}

#pragma mark: NetworkDelegate Methods

//-(void)serverResponse:(id)response {
//    NSLog(@"SERVER RESPONSE :%@",(NSArray *)[response objectForKey:@"listings"]);
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kHIDEREGISTRATION object:nil userInfo:nil];
//    
//}

-(void) didFailToRetrieveToken:(NSString *)msg {
    //NSLog(@"ERROR FOR FBLOGIN");
}

@end
