//
//  RootViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
@import GoogleMobileAds;
@import Firebase;

@interface RootViewController : UIViewController
@property (strong, nonatomic) FIRDatabaseReference *ref;
-(void) callLogout;
@end
