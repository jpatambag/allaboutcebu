//
//  RootViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

@import GoogleMobileAds;

#import "RootViewController.h"
#import "Global.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "WeatherController.h"
#import "EventsController.h"
#import "SearchPlaceController.h"
//#import "GADBannerView.h"
//#import "GADRequest.h"
#import "NetworkLayer.h"
#import "LatestListingsViewController.h"
#import "LatestEventsViewController.h"
#import "EventDetailsViewController.h"
#import "ListingDetailsViewController.h"
#import "LatestWeatherViewController.h"
#import "WeatherDetailsController.h"
#import "LatestActivityCollectionCell.h"
#import "LatestListingCollectionCell.h"
#import "NSDate+Helper.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "RegistrationViewController.h"
#import "SlideNavigationController.h"
#import "Events.h"
#import "Listing.h"
#import "Weather.h"

@interface RootViewController () <GADBannerViewDelegate,NetworkLayerDelegate,LatestEventsViewControllerDelegate, LatestListingsViewControllerDelegate,LatestWeatherViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate> {
        NetworkLayer *network;
}
@property (strong, nonatomic) IBOutlet GADBannerView *googleBannerVIew;
@property (nonatomic, strong) LoginViewController *loginView;
@property (nonatomic, strong) WeatherController *weatherViewController;
@property (nonatomic, strong) SearchPlaceController *listingViewController;
@property (nonatomic, strong) EventsController *eventsViewController;

@property (nonatomic, strong) LatestListingsViewController *latestListings;
@property (nonatomic, strong) LatestEventsViewController *latestEvents;
@property (nonatomic,strong) LatestWeatherViewController *latestWeather;
@property (nonatomic,strong) EventDetailsViewController *eventDetails;
@property (nonatomic, strong) ListingDetailsViewController *listingsDetails;
@property (nonatomic, strong) WeatherDetailsController *weatherDetails;
@property (nonatomic,strong) RegistrationViewController *registrationController;

@property (strong, nonatomic) IBOutlet UIView *viewLatestListings;
@property (strong, nonatomic) IBOutlet UIView *viewLatestWeather;
@property (strong, nonatomic) IBOutlet UIView *viewLatestEvents;

//@property (nonatomic, strong) NSMutableArray *weatherData;
@property (nonatomic,strong) NSMutableDictionary *weatherData;
@property (nonatomic, strong) NSMutableArray *listingData;
@property (nonatomic, strong) NSMutableArray *eventsData;

@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;

#pragma Dashboard User Interface

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet UILabel *txtTitle;
@property (nonatomic,weak) IBOutlet UILabel *txtSunrise;
@property (nonatomic,weak) IBOutlet UILabel *txtSunset;
@property (nonatomic,weak) IBOutlet UILabel *txtTemp;
@property (nonatomic,weak) IBOutlet UILabel *txtHumidity;
@property (nonatomic,weak) IBOutlet UILabel *txtPressure;
@property (nonatomic,weak) IBOutlet UILabel *txtDay;
@property (nonatomic,weak) IBOutlet UILabel *txtTime;
@property (nonatomic,weak) IBOutlet UILabel *txtDate;
@property (nonatomic,weak) IBOutlet UIImageView *imgWeatherIcon;

@property (nonatomic, strong) IBOutlet UICollectionView *latestEventsCollection;
@property (nonatomic, strong) IBOutlet UICollectionView *latestListingsCollection;
@property (nonatomic, strong) Weather *weatherModel;
@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }

    if (!self.weatherData) {
        self.weatherData = [[NSMutableDictionary alloc] init];
    }
    
    if (!self.listingData) {
        self.listingData = [[NSMutableArray alloc] init];
    }
    
    if (!self.eventsData) {
        self.eventsData = [[NSMutableArray alloc] init];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideRegistrationView:) name:kHIDEREGISTRATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRegistrationForm) name:kSHOWREGISTRATION_FORM object:nil];
    
    
    
    if (!self.weatherViewController) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.weatherViewController = [storyboard instantiateViewControllerWithIdentifier:@"WeatherController"];
    }
    
    if (!self.listingViewController) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Places" bundle:nil];
        self.listingViewController = [storyboard instantiateViewControllerWithIdentifier:@"SearchPlaceController"];
    }
    
    if (!self.eventsViewController) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.eventsViewController = [storyboard instantiateViewControllerWithIdentifier:@"EventsController"];
    }
    
    if (!self.latestEvents) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.latestEvents = [storyboard instantiateViewControllerWithIdentifier:@"LatestEvents"];
        self.latestEvents.delegate = self;
    }
    
    if (!self.latestListings) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.latestListings = [storyboard instantiateViewControllerWithIdentifier:@"LatestListings"];
        self.latestListings.delegate = self;
    }
    
    if (!self.latestWeather) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.latestWeather = [storyboard instantiateViewControllerWithIdentifier:@"LatestWeather"];
        self.latestWeather.delegate = self;
    }
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:refreshControl];
    
    [self addBannerOnThisView];
    //[self checkForUserRegistered];
    
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
}

- (void)refreshData:(UIRefreshControl *)refreshControl
{
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSThread sleepForTimeInterval:3];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM d, h:mm a"];
            NSString *lastUpdate = [NSString stringWithFormat:@"Last updated on %@", [formatter stringFromDate:[NSDate date]]];
            
            refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdate];
            
            [self getDataFromApi];
            
            [refreshControl endRefreshing];
            
            //NSLog(@"refresh end");
        });
    });
}

-(void)updateTime
{
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self getDataFromApi];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) showFonts {
    
    NSArray *familyNames = [UIFont familyNames];
    for( NSString *familyName in familyNames ){
        printf( "Family: %s \n", [familyName UTF8String] );
        
        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
        for( NSString *fontName in fontNames ){
            printf( "\tFont: %s \n", [fontName UTF8String] );
            
        }
    }
}

-(IBAction) onBtnShowMenu {
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

-(void)adjustScrollViewContentSize {
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 314);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 218);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 180);
    }
    
    if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
        self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 395);
    }
}

-(void) addBannerOnThisView {
    self.googleBannerVIew.adUnitID = kGOOGLE_BANNER_ADD_UNIT;
    self.googleBannerVIew.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    self.googleBannerVIew.delegate = self;
    // Enable test ads on simulators.
    request.testDevices = @[@"d1c340abb06de75c6a29e08865770c8be343e606",@"c1589702d7b51e99ba56a98c3ccad9b85f75109f",@"59daea62306d7c61bbb77cb637e1e389b21ee23c"];
    [self.googleBannerVIew loadRequest:request];
}

-(void) checkForUserRegistered {
    
    //NSLog(@"navigation controller :%@",self.navigationController.viewControllers);
    
    //NSLog(@"kREGISTERED :%d",[[NSUserDefaults standardUserDefaults] objectIsForcedForKey:kREGISTERED]);
    
    if (![GlobalInstance defaultExistWithName:kREGISTERED]) {
        
        if (!self.loginView) {
            self.loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginController"];
        }
        
        UINavigationController *regNavigation = [[UINavigationController alloc] initWithRootViewController:self.loginView];
        regNavigation.navigationBarHidden = YES;
        regNavigation.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        regNavigation.modalPresentationStyle = UIModalPresentationCurrentContext;
        
        //AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        
        [self presentViewController:regNavigation
                                                animated:YES
                                              completion:nil];
        
    } else {
        [self getDataFromApi];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onBtnDidTapMenu:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            [self.navigationController pushViewController:self.weatherViewController animated:YES];
            break;
            
        case 1:
             //[self.navigationController pushViewController:self.listingViewController animated:YES];
            break;
            
        case 2:
             [self.navigationController pushViewController:self.eventsViewController animated:YES];
            break;
            
        default:
            break;
    }
}

#pragma mark - Notification Methods

-(void) hideRegistrationView:(NSNotification *)notification {
    
    //NSLog(@"hideRegistrationView :%@",notification.userInfo);
    
    NSDictionary *fbDataDict = [[NSDictionary alloc] initWithDictionary:[notification.userInfo dictionaryByReplacingNullsWithBlanks]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfileName" object:fbDataDict];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:fbDataDict forKey:kFBDATA];
    [defaults setObject:@"1" forKey:kREGISTERED];
    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self getDataFromApi];
}

-(void) showRegistrationForm {
    
    
    //if(!self.registrationController) {
        self.registrationController = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
    //}
    
    
    
    UINavigationController *regNavigation = [[UINavigationController alloc] initWithRootViewController:self.registrationController];
    regNavigation.navigationBarHidden = YES;
    regNavigation.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    regNavigation.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:regNavigation
                       animated:YES
                     completion:nil];
    
    

}

-(void) callLogout {
    [self showRegistrationForm];
}


- (void) adView: (GADBannerView*) view didFailToReceiveAdWithError: (GADRequestError*) error {
    
}

- (void) adViewDidReceiveAd: (GADBannerView*) view {
    
}

#pragma mark API Calls

-(void) getDataFromApi {

    //Weather
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"weather"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        self.weatherModel = [Weather weatherWithObject:snapshot.value];
        [self populateWeather:self.weatherModel];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
    
    
    //Events
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"events"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *eventsArray = [[NSMutableArray alloc] init];

        for (FIRDataSnapshot* children in snapshot.children) {
            Events *model = [Events eventWithObject:children.value];
            [eventsArray addObject:model];
        }
        
        [self.eventsData removeAllObjects];
        [self.latestEvents populateDataOnView:eventsArray];
        [self.eventsData addObjectsFromArray:eventsArray];
        [self.latestEventsCollection reloadData];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];

    //Listings
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"listing"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSMutableArray *listingArray = [[NSMutableArray alloc] init];
        
        for (FIRDataSnapshot* children in snapshot.children) {
            Listing *model = [Listing listingWithObject:children.value];
            [listingArray addObject:model];
        }
        
        [self.listingData removeAllObjects];
        [self.latestListings populateDataOnView:listingArray];
        [self.listingData addObjectsFromArray:listingArray];
        [self.latestListingsCollection reloadData];
        
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch listings list" message:[error localizedDescription]];
    }];
    
    
}


#pragma mark: NetworkDelegate Methods

-(void)serverResponse:(id)response tag:(int)apiTag{
    
    NSLog(@"SERVER RESPONSE FOR :%d is :%@",apiTag,response);
    
    switch (apiTag) {
        case 0:{
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
                self.latestWeather.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
                self.latestWeather.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
                self.latestWeather.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
                self.latestWeather.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            /*if ([(NSArray *)response count] > 0) {
                
                
                [self.latestWeather populateDataOnView:response];
                [self populateWeather:response];
            
            } else {
                [self.latestWeather populateDataOnView:[NSArray array]];
            }*/
            
            NSArray *dataResponse = [[response objectForKey:@"daily"] objectForKey:@"data"];
            [self populateWeather:[dataResponse objectAtIndex:0]];
            //[self.latestWeather populateDataOnView:[dataResponse objectAtIndex:0]];
            
            
            //[self.viewLatestWeather addSubview:self.latestWeather.view];
            
            
            break;
            
        }
            
        case 1:
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
                self.latestListings.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
                self.latestListings.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
                self.latestListings.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
                self.latestListings.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            [self.listingData removeAllObjects];
            
            if ([(NSArray *)response count] > 0) {
                [self.latestListings populateDataOnView:[NSArray arrayWithObjects:[response objectAtIndex:0],[response objectAtIndex:1],[response objectAtIndex:2],[response objectAtIndex:3],[response objectAtIndex:4], nil]];
            
                [self.listingData addObjectsFromArray:[NSArray arrayWithObjects:[response objectAtIndex:0],[response objectAtIndex:1],[response objectAtIndex:2],[response objectAtIndex:3],[response objectAtIndex:4], nil]];
            } else {
                [self.latestListings populateDataOnView:[NSArray array]];
            }
            
            
            [self.latestListingsCollection reloadData];
            //[self.viewLatestListings addSubview:self.latestListings.view];

            break;
            
        case 2:
            
            //self.latestEvents.items = [NSMutableArray arrayWithArray:(NSArray *)response];
            //[self.latestEvents.carousel reloadData];
            //[self.latestEvents.view removeFromSuperview];
            //[self.latestEvents.view removeConstraints:[self.latestEvents.view constraints]];

            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_5]) {
               //self.latestEvents.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6]) {
                //self.latestEvents.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_6_PLUS]) {
                //self.latestEvents.view.frame = CGRectMake(0, 0, 380, 250);
            }
            
            if ([[GlobalInstance checkScreen] isEqualToString:kIPHONE_4_4S]) {
                //self.latestEvents.view.frame = CGRectMake(0, 0, 300, 139);
            }
            
            [self.eventsData removeAllObjects];
            
            [self.eventsData addObjectsFromArray:(NSArray *)response];
            [self.latestEventsCollection reloadData];
            
            [self.latestEvents populateDataOnView:(NSArray *)response];
            //[self.viewLatestEvents addSubview:self.latestEvents.view];
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark - latestEvents Delegate

-(void) didTapShowEventDetail:(Events *)data {
    if (!self.eventDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.eventDetails = [storyboard instantiateViewControllerWithIdentifier:@"EventDetailsController"];
    }
    
    self.eventDetails.event = data;
    
    self.navigationController.title = @"";
    [self.navigationController pushViewController:self.eventDetails animated:YES];

}

#pragma mark - latestlistings Delegate

-(void) didTapShowListingDetail:(Listing *)data {
    if (!self.listingsDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.listingsDetails = [storyboard instantiateViewControllerWithIdentifier:@"ListingDetails"];
    }
    
    self.listingsDetails.listing = data;
    
    [self.navigationController pushViewController:self.listingsDetails animated:YES];
}

#pragma mark - WeatherDetails Delegate

-(void) didTapShowWeatherDetail:(NSDictionary *)data {
    if (!self.weatherDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Weather" bundle:nil];
        self.weatherDetails = [storyboard instantiateViewControllerWithIdentifier:@"WeatherDetails"];
    }
    
    [GlobalInstance setViewFromDashboard:NO];
    
    self.weatherDetails.weatherObject = self.weatherModel;
    
    [self.navigationController pushViewController:self.weatherDetails animated:YES];
}

#pragma mark - UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSUInteger items = 0;
    
    if (collectionView == self.latestEventsCollection) {
        items = [self.eventsData count];
    }
    
    if (collectionView == self.latestListingsCollection) {
        items = [self.listingData count];
    }
    
    return items;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    id theCell;
    
    if (collectionView.tag == 0) {
//        LatestListingCollectionCell *cell = [collectionView
//                                              dequeueReusableCellWithReuseIdentifier:@"LatestListingCollectionCell"
//                                              forIndexPath:indexPath];
        
        theCell = (LatestListingCollectionCell *)[collectionView
                                             dequeueReusableCellWithReuseIdentifier:@"LatestListingCollectionCell"
                                             forIndexPath:indexPath];
        
        [theCell bindDataOnGridCell:[self.listingData objectAtIndex:indexPath.row]];
        //return cell;

    }
    
    if (collectionView.tag == 1) {
        
//        LatestActivityCollectionCell *cell = [collectionView
//                                              dequeueReusableCellWithReuseIdentifier:@"LatestActivityCollectionCell"
//                                              forIndexPath:indexPath];

        theCell = (LatestActivityCollectionCell *)[collectionView
                                              dequeueReusableCellWithReuseIdentifier:@"LatestActivityCollectionCell"
                                              forIndexPath:indexPath];
        
        [theCell bindDataOnGridCell:[self.eventsData objectAtIndex:indexPath.row]];
        
        //return cell;
    }
    
    return theCell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.viewLatestListings.frame.size;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
     if (collectionView.tag == 0) {
         [self didTapShowListingDetail:[self.listingData objectAtIndex:indexPath.row]];
     }
    
     if (collectionView.tag == 1) {
         [self didTapShowEventDetail:[self.eventsData objectAtIndex:indexPath.row]];
     }
}

#pragma mark - Latest Weather

-(void) populateWeather:(Weather *) data{
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    NSString *bgIcon = data.dashboardIcon;
    
    [self.imgWeatherIcon setImageWithURL:[NSURL URLWithString:bgIcon] placeholderImage:nil];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtTitle.text = [self.weatherData objectForKey:@"summary"];
    
    
    NSDate *sunriseTime = [NSDate dateWithTimeIntervalSince1970:[data.sunriseTime floatValue]];
    
    NSDate *sunsetTime = [NSDate dateWithTimeIntervalSince1970:[data.sunsetTime floatValue]];
    
    self.txtSunrise.text = [NSString stringWithFormat:@"Sunrise : %@",[NSDate stringFromDate:sunriseTime withFormat:@"hh:mm aa"]];
    self.txtSunset.text = [NSString stringWithFormat:@"Sunset : %@",[NSDate stringFromDate:sunsetTime withFormat:@"hh:mm aa"]];
    self.txtTemp.text = [NSString stringWithFormat:@"%d °C", [data.apparentTemperature intValue]];
    self.txtHumidity.text = [NSString stringWithFormat:@"Humidity : %@ %%",data.humidity];
    self.txtPressure.text = [NSString stringWithFormat:@"Pressure :%@ hpa",data.pressure];
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
    NSString *bgImageURL = data.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgIcon setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgIcon.image = image;
                                         
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];

}

/*-(void) populateWeather:(NSMutableDictionary *) data{
    
    [self.weatherData removeAllObjects];
    //[self.weatherData addObjectsFromArray:array];
    [self.weatherData setDictionary:data];
    
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@/%@.png",ROOT_URL,kWEATHER_IMAGE_ICON,[self.weatherData objectForKey:@"icon"]];
    [self.imgWeatherIcon setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@""]];
    
    self.txtTitle.text = [self.weatherData objectForKey:@"summary"];
   
    
    NSDate *sunriseTime = [NSDate dateWithTimeIntervalSince1970:[[self.weatherData objectForKey:@"sunriseTime"] floatValue]];
    
    NSDate *sunsetTime = [NSDate dateWithTimeIntervalSince1970:[[self.weatherData objectForKey:@"sunsetTime"] floatValue]];
    
    self.txtSunrise.text = [NSString stringWithFormat:@"Sunrise : %@",[NSDate stringFromDate:sunriseTime withFormat:@"hh:mm aa"]];
    self.txtSunset.text = [NSString stringWithFormat:@"Sunset : %@",[NSDate stringFromDate:sunsetTime withFormat:@"hh:mm aa"]];
    self.txtTemp.text = [NSString stringWithFormat:@"%0.1f°C", [[self.weatherData objectForKey:@"apparentTemperatureMax"] floatValue]];
    self.txtHumidity.text = [NSString stringWithFormat:@"Humidity : %@ %%",[self.weatherData objectForKey:@"humidity"]];
    self.txtPressure.text = [NSString stringWithFormat:@"Pressure :%@ hpa",[self.weatherData objectForKey:@"pressure"]];
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
    NSString *bgImageURL = [NSString stringWithFormat:@"%@%@/weather.png",ROOT_URL,kGET_DASHBOARD_WEATHER];
    //    [self.imgIcon setImageWithURL:[NSURL URLWithString:bgImageURL] placeholderImage:[UIImage imageNamed:@""]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgIcon setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgIcon.image = image;
                                         
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];
    

}*/

-(IBAction)onBtnShowWeatherDetails:(id)sender {
    
    [self didTapShowWeatherDetail:self.weatherData];
}

@end
