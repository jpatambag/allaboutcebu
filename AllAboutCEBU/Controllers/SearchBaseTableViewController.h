//
//  SearchBaseTableViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 12/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeCell.h"
@class SearchModel;

extern NSString *const kSearchCellIdentifier;

@interface SearchBaseTableViewController : UITableViewController
- (void)configureCell:(homeCell *)cell forCategoryContent:(SearchModel *)item;
@end
