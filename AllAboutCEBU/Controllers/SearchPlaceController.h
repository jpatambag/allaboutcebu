//
//  SearchPlaceController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 21/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchBaseTableViewController.h"

@interface SearchPlaceController : SearchBaseTableViewController
@property (nonatomic,strong) NSDictionary *placesObject;
@property (nonatomic,strong) NSMutableArray *categoryData;
@end
