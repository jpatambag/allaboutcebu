//
//  SearchResultViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 13/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "SearchBaseTableViewController.h"

@interface SearchResultViewController : SearchBaseTableViewController
@property (nonatomic, strong) NSMutableArray *searchFilters;

@end
