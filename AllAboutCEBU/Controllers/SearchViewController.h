//
//  SearchViewController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 1/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

//#import <TCTableViewSearchController/TCTableViewSearchController.h>
#import "TCTableViewSearchController.h"
@import Firebase;

@interface SearchViewController : TCTableViewSearchController <TCTableViewSearchControllerDelegate>

@property (nonatomic,strong) NSDictionary *placesObject;
@property (nonatomic,strong) NSMutableArray *categoryData;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, strong) FIRDatabaseReference *ref;
@end
