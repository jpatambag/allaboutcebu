//
//  SearchViewController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 1/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "SearchViewController.h"
#import "NetworkLayer.h"
#import "Constants.h"
#import "homeCell.h"
#import "SearchModel.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "ListingDetailsViewController.h"
#import "Global.h"
#import "CategoryItems.h"

@interface SearchViewController () <NetworkLayerDelegate>{
    NetworkLayer *network;
}
@property (nonatomic, strong) NSDictionary *categoryObject;
@property (nonatomic, strong) IBOutlet UITableView *tblList;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic,strong) ListingDetailsViewController *listingDetails;
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
    
    /*if (!self.dataArray) {
        self.dataArray = [[NSMutableArray alloc] init];
    } else {
        [self.dataArray removeAllObjects];
    }*/
    
    self.dataArray = [[NSMutableArray  alloc] init];
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    [self populateInitialDataOnSearch];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateViewTitle];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateViewTitle];
    
}

-(void) updateViewTitle {
    
    if (self.category.length > 0) {
        self.title = [self.category uppercaseString];
    } else {
        self.title = @"Places";
    }
}

-(void)populateInitialDataOnSearch {
    
    [self.dataArray removeAllObjects];
    
    [[[[self.ref child:@"cebu"] child:@"category-listings"] child:self.category] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        for (FIRDataSnapshot* children in snapshot.children) {
            CategoryItems *model = [CategoryItems itemWithObject:children.value];
            [self.dataArray addObject:model];
        }
        
        [self.tableView reloadData];

    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (!self.searchController.active) ? self.dataArray.count : self.filteredResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    homeCell *cell = (homeCell *) [tableView dequeueReusableCellWithIdentifier:@"homeCell"];

    CategoryItems *items = (!self.searchController.active) ? self.dataArray[indexPath.row]: self.filteredResults[indexPath.row];
    
    [cell updateCellDataWithItem:items];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.listingDetails) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.listingDetails = [storyboard instantiateViewControllerWithIdentifier:@"ListingDetails"];
    }
    
    Listing *listing = (!self.searchController.active) ? self.dataArray[indexPath.row]: self.filteredResults[indexPath.row];
    
    self.listingDetails.listing = listing;
    
    [self.navigationController pushViewController:self.listingDetails animated:YES];
    
    
    NSLog(@"CHECK SEARCH DATA :%@",[self.dataArray objectAtIndex:indexPath.row]);
}

#pragma mark - TCSearchingTableViewControllerDelegate

- (void)updateSearchResultsForSearchingTableViewController:(TCTableViewSearchController *)searchingTableViewController withCompletion:(TCSearchBlock)completion {
    
    NSArray *propertiesArray = [NSArray arrayWithObjects:@"name", @"address", nil];
    
    completion(self.dataArray, propertiesArray, [CategoryItems place]);

}

- (NSArray *)scopeBarTitles {
    return @[@"All", @"Name", @"Breed", @"Owner", @"Birth Year"];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    if ([self.filteredResults count] == 0) {
        [self searchPlaceWithKeyword:searchBar.text];
    }
}

-(void)searchPlaceWithKeyword:(NSString *)keyword {
    //api/listing/?Name__PartialMatch=%@&category=%@&__limit[]=%@
    [self.dataArray removeAllObjects];
    //NSString *url = [NSString stringWithFormat:,keyword,,@"5"];
    
    /*NSDictionary *params = @{
                             @"Name__PartialMatch" : keyword,
                             @"category" : [self.placesObject objectForKey:@"Name"],
                             @"__limit[]" : @"5"
                             };
    
    [network requestDataFromServer:kSEARCH_BY_CATEGORY andParams:params forTag:0];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
