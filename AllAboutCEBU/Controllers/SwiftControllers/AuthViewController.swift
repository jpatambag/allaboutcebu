//
//  Copyright (c) 2016 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import UIKit
import Firebase
import FirebaseAuthUI
import FirebaseGoogleAuthUI
import FirebaseFacebookAuthUI

let kFirebaseTermsOfService = URL(string: "https://firebase.google.com/terms/")!

// Your Google app's client ID, which can be found in the GoogleService-Info.plist file
// and is stored in the `clientID` property of your FIRApp options.
// Firebase Google auth is built on top of Google sign-in, so you'll have to add a URL
// scheme to your project as outlined at the bottom of this reference:
// https://developers.google.com/identity/sign-in/ios/start-integrating
let kGoogleAppClientID = (FIRApp.defaultApp()?.options.clientID)!

// Your Facebook App ID, which can be found on developers.facebook.com.
let kFacebookAppID     = "845089562213635"

/// A view controller displaying a basic sign-in flow using FIRAuthUI.
class AuthViewController: UIViewController, CAPSPageMenuDelegate, FIRAuthUIDelegate {

  fileprivate var authStateDidChangeHandle: FIRAuthStateDidChangeListenerHandle?
  
  fileprivate(set) var auth: FIRAuth? = FIRAuth.auth()
  fileprivate(set) var authUI: FIRAuthUI? = FIRAuthUI.authUI()
  
  @IBOutlet fileprivate var nameLabel: UILabel!
  @IBOutlet fileprivate var emailLabel: UILabel!
  @IBOutlet fileprivate var uidLabel: UILabel!
  @IBOutlet fileprivate var profileImage: UIImageView!
  @IBOutlet var viewContainer: UIView!
  var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViewControllers()
    }
    
    func checkUserLogged() {
        self.authenticateUser()
        /*let providers: [FIRAuthProviderUI] = [
            FIRGoogleAuthUI(clientID: kGoogleAppClientID)!,
            FIRFacebookAuthUI(appID: kFacebookAppID)!,
            ]
        self.authUI?.signInProviders = providers
        self.authUI?.termsOfServiceURL = kFirebaseTermsOfService
        
        self.authStateDidChangeHandle =
            self.auth?.addAuthStateDidChangeListener(self.updateUI(auth:user:))*/
    }
    
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    checkUserLogged()
    // If you haven't set up your authentications correctly these buttons
    // will still appear in the UI, but they'll crash the app when tapped.
    
    
   
  }
    
    func logoutUser() {
        do {
            try self.auth?.signOut()
        } catch let error {
            // Again, fatalError is not a graceful way to handle errors.
            // This error is most likely a network error, so retrying here
            // makes sense.
            fatalError("Could not sign out: \(error)")
        }
    }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if let handle = self.authStateDidChangeHandle {
      self.auth?.removeStateDidChangeListener(handle)
    }
  }
    
    func authPickerViewControllerForAuthUI(authUI: FIRAuthUI) -> FIRAuthPickerViewController {
        return CustomAuthPickerViewController(authUI: authUI)
    }
    
    func initializeSession() {
        let authUI = FIRAuthUI.authUI()
        let googleAuthUI = FIRGoogleAuthUI(clientID: FIRApp.defaultApp()!.options.clientID)
        //let facebookAuthUI = FIRFacebookAuthUI(appID: NSBundle.mainBundle().infoDictionary!["FacebookAppID"] as! String)
        authUI!.signInProviders = [googleAuthUI!]
        // let authViewController =  authPickerViewControllerForAuthUI(authUI!)
        let authViewController = authUI!.authViewController()
        self.present(authViewController, animated: true, completion: nil)
    }
  
    /** @fn authUI:didSignInWithUser:error:
     @brief Message sent after the sign in process has completed to report the signed in user or
     error encountered.
     @param authUI The @c FIRAuthUI instance sending the messsage.
     @param user The signed in user if the sign in attempt was successful.
     @param error The error that occured during sign in, if any.
     */
    public func authUI(_ authUI: FIRAuthUI, didSignInWith user: FIRUser?, error: Error?) {
        
    }
    
    func setUpViewControllers() {
        var controllerArray : [UIViewController] = []
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let firstController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        firstController.view.frame = CGRect(x: 0, y: 0, width: self.viewContainer.frame.size.width, height: self.viewContainer.frame.size.height)
        firstController.view.backgroundColor = UIColor.red
        firstController.title = "FAVORITES"
        firstController.view.layoutIfNeeded()
        controllerArray.append(firstController)
        
        let secondController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        secondController.view.frame = CGRect(x: 0, y: 0, width: self.viewContainer.frame.size.width, height: self.viewContainer.frame.size.height)
        secondController.title = "RECENTS"
        secondController.view.layoutIfNeeded()
        controllerArray.append(secondController)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor.clear),
            .selectionIndicatorColor(UIColor.clear),
            .menuMargin(0.0),
            .menuHeight(40.0),
            .menuItemWidth(UIScreen.main.bounds.size.width / CGFloat(controllerArray.count)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuItemFont(UIFont(name: "Helvetica", size: 12.0)!),
            .menuItemSeparatorRoundEdges(false),
            .titleTextSizeBasedOnMenuItemWidth(false),
            .selectionIndicatorHeight(2.0),
            //.MenuItemSeparatorPercentageHeight(0.1)
            .selectionIndicatorColor(UIColor.black)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.viewContainer.frame.width, height: self.viewContainer.frame.height), pageMenuOptions: parameters)
        self.pageMenu?.view.layoutIfNeeded()
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu!.menuScrollView.sizeThatFits(CGSize(width: self.view.frame.size.width, height: 40.0))
        pageMenu!.menuScrollView.isScrollEnabled = false
        pageMenu!.controllerScrollView.isScrollEnabled = true
        self.viewContainer.addSubview(pageMenu!.view)

        
    }
    
  @IBAction func startPressed(_ sender: AnyObject) {
    let controller = self.authUI!.authViewController()
    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    backgroundImage.image = UIImage(named: "signup_bg")
    controller.view.insertSubview(backgroundImage, at: 0)
    self.present(controller, animated: true, completion: nil)
  }
    
    func authenticateUser() {
        let controller = self.authUI!.authViewController()
        controller.view.backgroundColor = UIColor(patternImage: UIImage(named: "signup_bg")!)
        self.present(controller, animated: true, completion: nil)
    }
  
  @IBAction func signOutPressed(_ sender: AnyObject) {
    do {
     try self.auth?.signOut()
    } catch let error {
      // Again, fatalError is not a graceful way to handle errors.
      // This error is most likely a network error, so retrying here
      // makes sense.
      fatalError("Could not sign out: \(error)")
    }
  }
  
  // Boilerplate
  func updateUI(auth: FIRAuth, user: FIRUser?) {
    if let user = user {
      //self.signOutButton.enabled = true
      //self.startButton.enabled = false
      
      self.nameLabel.text = "Name: " + (user.displayName ?? "(null)")
      self.emailLabel.text = "Email: " + (user.email ?? "(null)")
      //self.uidLabel.text = "UID: " + user.uid
    } else {
      //self.signOutButton.enabled = false
      //self.startButton.enabled = true
      
      self.nameLabel.text = ""
      self.emailLabel.text = ""
      self.authenticateUser()
      //self.uidLabel.text = "UID"
    }
  }
  
  override func viewWillLayoutSubviews() {
    //self.topConstraint.constant = self.topLayoutGuide.length
    pageMenu!.view.layoutIfNeeded()
  }
}
