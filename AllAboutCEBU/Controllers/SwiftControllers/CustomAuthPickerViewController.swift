//
//  CustomAuthPickerViewController.swift
//  AllAboutCEBU
//
//  Created by Joseph on 18/9/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuthUI
import FirebaseFacebookAuthUI
import FBSDKLoginKit
import FirebaseDatabase
import FirebaseGoogleAuthUI
import FirebaseFacebookAuthUI

class CustomAuthPickerViewController: FIRAuthPickerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor =  .blue
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
