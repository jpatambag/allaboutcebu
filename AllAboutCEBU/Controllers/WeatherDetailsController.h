//
//  WeatherDetailsController.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 12/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Weather.h"
@import Firebase;

@interface WeatherDetailsController : UIViewController
@property (nonatomic, strong) Weather *weatherObject;
@property (nonatomic, strong) NSArray *forecastObject;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (nonatomic,assign) BOOL isDashboard;

@end
