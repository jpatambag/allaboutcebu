//
//  WeatherDetailsController.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 12/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "WeatherDetailsController.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "CollectionCell.h"
#import "NetworkLayer.h"
#import "ForcastCell.h"
#import "SlideNavigationController.h"
#import "Global.h"
#import "WeatherForecast.h"

@interface WeatherDetailsController () <UICollectionViewDataSource, UICollectionViewDelegate, NetworkLayerDelegate,UITableViewDataSource,UITableViewDelegate,SlideNavigationControllerDelegate> {
    
     NetworkLayer *network;
}
@property (nonatomic,strong) IBOutlet UIImageView *weatherBgImage;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *forecastData;
@property (nonatomic,strong) IBOutlet UITableView *tblForcast;

#pragma Dashboard User Interface

@property (nonatomic, weak) IBOutlet UIImageView *imgIcon;
@property (nonatomic,weak) IBOutlet UILabel *txtTitle;
@property (nonatomic,weak) IBOutlet UILabel *txtSunrise;
@property (nonatomic,weak) IBOutlet UILabel *txtSunset;
@property (nonatomic,weak) IBOutlet UILabel *txtTemp;
@property (nonatomic,weak) IBOutlet UILabel *txtHumidity;
@property (nonatomic,weak) IBOutlet UILabel *txtPressure;
@property (nonatomic,weak) IBOutlet UILabel *txtDay;
@property (nonatomic,weak) IBOutlet UILabel *txtTime;
@property (nonatomic,weak) IBOutlet UILabel *txtDate;
@property (nonatomic,weak) IBOutlet UIImageView *imgWeatherIcon;

@end

@implementation WeatherDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!network) {
        network = [[NetworkLayer alloc] init];
        network.delegate = self;
    }
    
    if (!self.forecastData) {
        self.forecastData = [[NSMutableArray alloc] init];
    }
    
    self.ref = [[FIRDatabase database] reference];
    [self.ref keepSynced:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getForecastData];
    self.title = @"Weather Details";
    
}

-(void) viewWillAppear:(BOOL) animated {
    
    [super viewWillAppear:animated];
}

-(void)updateTime
{
    
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMMM"];
    
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    
}

-(void) populateDataOnView {
    
    //NSDictionary *filter = [self.dataDict dictionaryByReplacingNullsWithBlanks];
    
    [self.forecastData removeAllObjects];
    [self.forecastData addObjectsFromArray:self.weatherObject.forecast];
    [self.tblForcast reloadData];
    
    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"PHT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* dateinitial = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
    //NSDate *dateinitial = [NSDate date];
    NSString *timeString = [NSDate stringFromDate:dateinitial withFormat:@"hh:mm aa"];
    NSString *dayString = [NSDate stringFromDate:dateinitial withFormat:@"EEEE"];
    NSString *dateString = [NSDate dayOfTheMonthToday];
    NSString *monthString = [NSDate stringFromDate:dateinitial withFormat:@"MMM"];
    
    NSString *imageURL = _weatherObject.dashboardIcon;
    [self.imgWeatherIcon setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@""]];
    
    self.txtTitle.text = _weatherObject.summary;
    
    
    NSDate *sunriseTime = [NSDate dateWithTimeIntervalSince1970:[_weatherObject.sunriseTime floatValue]];
    
    NSDate *sunsetTime = [NSDate dateWithTimeIntervalSince1970:[_weatherObject.sunsetTime floatValue]];
    
    self.txtSunrise.text = [NSString stringWithFormat:@"Sunrise : %@",[NSDate stringFromDate:sunriseTime withFormat:@"hh:mm aa"]];
    self.txtSunset.text = [NSString stringWithFormat:@"Sunset : %@",[NSDate stringFromDate:sunsetTime withFormat:@"hh:mm aa"]];
    self.txtTemp.text = [NSString stringWithFormat:@"%d °C", [_weatherObject.apparentTemperature intValue]];
    self.txtHumidity.text = [NSString stringWithFormat:@"Humidity : %@ %%",_weatherObject.humidity];
    self.txtPressure.text = [NSString stringWithFormat:@"Pressure :%@ hpa",_weatherObject.pressure];
    self.txtDay.text = dayString;
    self.txtDate.text = [NSString stringWithFormat:@"%@ %@",monthString,dateString];
    self.txtTime.text = timeString;
    //NSLog(@"filter: %@", filter);
    
    NSString *imageURL2 = _weatherObject.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL2]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.weatherBgImage setImageWithURLRequest:request
                     placeholderImage:[UIImage imageNamed:@"placeholder"]
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      weakSelf.weatherBgImage.image = image;
                                  });
                                  
                              } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                  NSLog(@"failed loading image: %@", error);
                              }];

    
    
}


#pragma mark API Calls
-(void) getForecastData {
//        [network requestWeatherData:[NSString stringWithFormat:@"%@",KFORECAST_IO_URL] andParams:nil forTag:0];
    
    //Weather
    [[[[self.ref child:@"cebu"] child:@"dashboard"] child:@"weather"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        self.weatherObject = [Weather weatherWithObject:snapshot.value];
        self.forecastObject = self.weatherObject.forecast;
        [self populateDataOnView];
        
    } withCancelBlock:^(NSError * _Nonnull error) {
        [GlobalInstance showAlert:@"Unable To fetch events list" message:[error localizedDescription]];
    }];
    
}

-(void) refreshForecastData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        [self getForecastData];
        dispatch_async(dispatch_get_main_queue(), ^{ // 2
            
        });
    });
}


#pragma mark: NetworkDelegate Methods

-(void)serverResponse:(id)response tag:(int)apiTag{
    
    
    if ([(NSDictionary *)response count] > 0) {
        /*[self.forecastData removeAllObjects];
        
        NSArray *dataResponse = [[response objectForKey:@"daily"] objectForKey:@"data"];
        
        NSArray *objectResponse = [NSArray arrayWithObjects:
                                   [dataResponse objectAtIndex:1],
                                   [dataResponse objectAtIndex:2],
                                   [dataResponse objectAtIndex:3],
                                   [dataResponse objectAtIndex:4],
                                   [dataResponse objectAtIndex:5],
                                   [dataResponse objectAtIndex:6],
                                   [dataResponse objectAtIndex:7]
                                   ,nil];
        
        self.dataDict =[dataResponse objectAtIndex:0];
        
        [self.forecastData addObjectsFromArray:objectResponse];
        [self.tblForcast reloadData];
        [self updateLatestWeather];*/
        
    } else {
        //[self.latestWeather populateDataOnView:[NSArray array]];
    }
    
    //NSLog(@"weather forecast list :%@",self.forecastData);
}


-(IBAction)reloadBackground:(id)sender {
    [self populateDataOnView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDelegate 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.forecastData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"ForcastCell";
    
    ForcastCell *cell = (ForcastCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor = [UIColor clearColor];
    
    WeatherForecast *forecastModel = [WeatherForecast forecastWithData:[self.forecastData objectAtIndex:indexPath.row]];
    
    [cell bindData:forecastModel];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"data selected :%@",[self.forecastData objectAtIndex:indexPath.row]);
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return [GlobalInstance viewFromDashboard] ? YES: NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
