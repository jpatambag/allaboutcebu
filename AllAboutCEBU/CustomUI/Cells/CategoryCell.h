//
//  CategoryCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/8/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UIImageView *catImage;
@property (nonatomic,weak) IBOutlet UILabel *catTitle;
@property (nonatomic,weak) IBOutlet UILabel *catDesc;
-(void) bindDataOnCell:(NSDictionary *)object;
@end
