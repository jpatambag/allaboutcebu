//
//  CategoryCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/8/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "CategoryCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"

@implementation CategoryCell
-(void) bindDataOnCell:(NSDictionary *)object {
    NSDictionary *filter = [object dictionaryByReplacingNullsWithBlanks];
    self.catTitle.text = [filter objectForKey:@"Name"];
    self.catDesc.text = [filter objectForKey:@"Description"];
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[filter objectForKey:@"ImgListView"]];
    [self.catImage setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

}
@end
