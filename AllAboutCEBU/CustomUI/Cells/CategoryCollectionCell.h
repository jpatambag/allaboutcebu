//
//  CategoryCollectionCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCatBg;
@property (weak, nonatomic) IBOutlet UILabel *lblCatTitle;


-(void) updateCellContent:(NSDictionary *)data;
@end
