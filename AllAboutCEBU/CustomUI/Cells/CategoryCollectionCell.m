//
//  CategoryCollectionCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "CategoryCollectionCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "NSDate+Helper.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Categories.h"

@implementation CategoryCollectionCell

- (void)awakeFromNib {
    // Initialization code
}

-(void) updateCellContent:(Categories *)data {

    self.lblCatTitle.text = data.name;
    [self.imgCatBg setImageWithURL:[NSURL URLWithString:data.categoryImg] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

}

@end
