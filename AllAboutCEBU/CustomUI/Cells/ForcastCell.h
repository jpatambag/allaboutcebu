//
//  ForcastCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 18/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForcastCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *lblDay;
@property (nonatomic,strong) IBOutlet UIImageView *weatherIcon;
@property (nonatomic,strong) IBOutlet UILabel *lblTemp;
@property (nonatomic,strong) IBOutlet UILabel *lblpressure;
-(void) bindData:(NSDictionary *)data;
@end
