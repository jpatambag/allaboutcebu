//
//  ForcastCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 18/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "ForcastCell.h"
#import "NSDictionary+NullReplacement.h"
#import "NSArray+NullReplacement.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "WeatherForecast.h"

@implementation ForcastCell

- (void)awakeFromNib {
    // Initialization code
}

-(void) bindData:(WeatherForecast *)forecast {
   
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EEEE"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT+8"]];
    
    NSDate *timestamp = [NSDate dateWithTimeIntervalSince1970:[forecast.time floatValue]];
    NSString *theDate = [dateFormat stringFromDate:timestamp];
    
    self.lblDay.text = theDate;

    NSString *imageURL = forecast.iconImg;
    [self.weatherIcon setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@""]];

    
    self.lblTemp.text = [NSString stringWithFormat:@"%d °C",[forecast.temperatureMax intValue]];
    self.lblpressure.text = [NSString stringWithFormat:@"%.2f",[forecast.pressure floatValue]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
