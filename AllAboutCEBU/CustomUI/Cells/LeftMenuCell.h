
#import <UIKit/UIKit.h>

@interface LeftMenuCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *menuText;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSelected;

-(void) bindDataonCell:(NSDictionary *)data;
@end
