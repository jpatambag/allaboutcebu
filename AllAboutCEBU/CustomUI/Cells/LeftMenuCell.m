
#import "LeftMenuCell.h"

@implementation LeftMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void) bindDataonCell:(NSDictionary *)data {
    self.menuText.text = [data objectForKey:@"title"];
}

@end
