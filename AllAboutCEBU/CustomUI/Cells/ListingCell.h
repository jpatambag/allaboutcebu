//
//  ListingCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgIcon;
-(void) populateDataOnCell:(NSDictionary *)data;
@end
