//
//  ListingCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "ListingCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"

@implementation ListingCell

- (void)awakeFromNib {
    // Initialization code
}

//-(void) populateDataOnCell:(NSDictionary *)data {
//    NSDictionary *filter = [data dictionaryByReplacingNullsWithBlanks];
//    
//    NSString *imageURL = [NSString stringWithFormat:@"%@%@%@%@",ROOT_URL,kGET_IMAGE,[filter objectForKey:@"ID"],@"/list-thumb"];
//    [self.imgIcon setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
//    
//    self.lblTitle.text = [filter objectForKey:@"Name"];
//
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
