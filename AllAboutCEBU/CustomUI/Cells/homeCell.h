//
//  homeCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryItems.h"
@class SearchModel;

@interface homeCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic, weak) IBOutlet UIImageView *catImage;
-(void) loadDataOnCell:(NSDictionary *)data;
-(void) updateCellData:(SearchModel *)model;
-(void) updateCellDataWithItem:(CategoryItems *)model;
@end
