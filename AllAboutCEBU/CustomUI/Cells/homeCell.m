//
//  homeCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "homeCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "SearchModel.h"

@implementation homeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) loadDataOnCell:(NSDictionary *)data {
    self.lblName.text = [data objectForKey:@"Name"];
    self.lblAddress.text = [data objectForKey:@"Address"];
 
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[data objectForKey:@"ImgListView"]];
    [self.catImage setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
}

-(void) updateCellData:(SearchModel *)model {
    self.lblName.text = model.Name;
    self.lblAddress.text = model.Address;
    
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,model.ImgListView];
    [self.catImage setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    
}

-(void) updateCellDataWithItem:(CategoryItems *)model {
    self.lblName.text = model.name;
    self.lblAddress.text = model.address;
    
    NSString *imageURL = model.listingImg;
    [self.catImage setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    

}

@end
