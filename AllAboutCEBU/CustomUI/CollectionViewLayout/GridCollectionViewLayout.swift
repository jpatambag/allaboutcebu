//
//  GridCollectionViewLayout.swift
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 9/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

import UIKit

protocol GridCollectionLayoutDelegate {
    // 1. Method to ask the delegate for the height of the image
    func collectionView(_ collectionView:UICollectionView, heightForObjectAtIndexPath indexPath:IndexPath , withWidth:CGFloat) -> CGFloat
    //    // 2. Method to ask the delegate for the height of the annotation text
    //    func collectionView(collectionView: UICollectionView, heightForAnnotationAtIndexPath indexPath: NSIndexPath, withWidth width: CGFloat) -> CGFloat
    
}

class GridCollectionLayoutAttributes:UICollectionViewLayoutAttributes {
    
    // 1. Custom attribute
    var itemHeight: CGFloat = 0.0
    
    // 2. Override copyWithZone to conform to NSCopying protocol
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! GridCollectionLayoutAttributes
        copy.itemHeight = itemHeight
        return copy
    }
    
    // 3. Override isEqual
    override func isEqual(_ object: Any?) -> Bool {
        if let attributtes = object as? GridCollectionLayoutAttributes {
            if( attributtes.itemHeight == itemHeight  ) {
                return super.isEqual(object)
            }
        }
        return false
    }
}

class GridCollectionViewLayout: UICollectionViewLayout {
    var delegate:GridCollectionLayoutDelegate!
    var numberOfColumns = 2
    var cellPadding: CGFloat = 6.0
    fileprivate var cache = [GridCollectionLayoutAttributes]()
    fileprivate var contentHeight:CGFloat  = 0.0
    fileprivate var contentWidth: CGFloat {
        let insets = collectionView!.contentInset
        return collectionView!.bounds.width - (insets.left + insets.right)
    }
    
    
    override class var layoutAttributesClass : AnyClass {
        return GridCollectionLayoutAttributes.self
    }
    
    override func prepare() {
        if cache.isEmpty {
            
            let columnWidth = contentWidth / CGFloat(numberOfColumns)
            var xOffset = [CGFloat]()
            for column in 0 ..< numberOfColumns {
                xOffset.append(CGFloat(column) * columnWidth )
            }
            var column = 0
            var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
            
            for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
                
                let indexPath = IndexPath(item: item, section: 0)
                
                let width = columnWidth - cellPadding*2
                let photoHeight = delegate.collectionView(collectionView!, heightForObjectAtIndexPath: indexPath , withWidth:width)
                //let annotationHeight = delegate.collectionView(collectionView!, heightForAnnotationAtIndexPath: indexPath, withWidth: width)
                let height = cellPadding +  photoHeight + cellPadding
                let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
                let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                
                // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
                let attributes = GridCollectionLayoutAttributes(forCellWith: indexPath)
                attributes.itemHeight = photoHeight
                attributes.frame = insetFrame
                cache.append(attributes)
                
                // 6. Updates the collection view content height
                contentHeight = max(contentHeight, frame.maxY)
                yOffset[column] = yOffset[column] + height

                
                column = column >= (numberOfColumns - 1) ? 0 : column + 1
            }
        }
    }
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes  in cache {
            if attributes.frame.intersects(rect ) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
}


