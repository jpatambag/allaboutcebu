//
//  GridViewLayout.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 9/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GridViewLayout : UICollectionViewFlowLayout
@property (nonatomic) UIEdgeInsets itemInsets;
@property (nonatomic) CGSize itemSize;
@property (nonatomic) CGFloat interItemSpacingY;
@property (nonatomic) NSInteger numberOfColumns;
@property (nonatomic, strong) NSDictionary *layoutInfo;
@end
