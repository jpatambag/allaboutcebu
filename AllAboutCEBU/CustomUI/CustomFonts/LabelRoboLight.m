//
//  LabelRoboLight.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LabelRoboLight.h"

@implementation LabelRoboLight

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.font = [UIFont fontWithName:@"Roboto-Light" size:self.font.pointSize];
        
    }
    return self;
}

@end
