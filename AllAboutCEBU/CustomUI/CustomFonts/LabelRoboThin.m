//
//  LabelRoboThin.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LabelRoboThin.h"

@implementation LabelRoboThin

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.font = [UIFont fontWithName:@"Roboto-Thin" size:self.font.pointSize];
        
    }
    return self;
}


@end
