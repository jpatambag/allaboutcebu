//
//  TextViewRoboThink.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 24/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "TextViewRoboThink.h"

@implementation TextViewRoboThink

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        self.font = [UIFont fontWithName:@"Roboto-Thin" size:self.font.pointSize];
    }
    return self;
}

@end
