//
//  LatestActivityCollectionCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "Events.h"

@interface LatestActivityCollectionCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblVenue;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
//+ (LatestActivityGridCell*)gridCell;
-(void)bindDataOnGridCell:(Events *)event;
@end
