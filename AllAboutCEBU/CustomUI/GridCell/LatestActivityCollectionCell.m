//
//  LatestActivityCollectionCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LatestActivityCollectionCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "Events.h"

@implementation LatestActivityCollectionCell

/*-(void) bindDataOnGridCell:(NSDictionary *)data {
    
    //NSDictionary *filter = [data dictionaryByReplacingNullsWithBlanks];
    
    //NSLog(@"filter LatestActivityCollectionCell : %@", filter);
    
    if ([[filter objectForKey:@"DateTime"] length] == 0) {
        self.lblDateTime.text = @"No Date";
    } else {
        NSDate *dateinitial = [NSDate dateFromString:[data objectForKey:@"DateTime"]];
        NSString *otherDateString = [NSDate stringFromDate:dateinitial withFormat:@"EEE, dd MMM yyyy @ hh:mm aa"];
        self.lblDateTime.text = otherDateString;
    }
    
    NSString *bgImageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[filter objectForKey:@"ImgDashboard"]];
    
    NSLog(@"image latest activity events :%@",bgImageURL);
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgItem.image = image;
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];
    
    
    self.lblTitle.text = [filter objectForKey:@"Title"];
    self.lblVenue.text = [filter objectForKey:@"Venue"];
    
}*/

-(void)bindDataOnGridCell:(Events *)event{
    if ([event.date length] == 0) {
        self.lblDateTime.text = @"No Date";
    } else {
        NSDate *dateinitial = [NSDate dateFromString:event.date];
        NSString *otherDateString = [NSDate stringFromDate:dateinitial withFormat:@"EEE, dd MMM yyyy @ hh:mm aa"];
        self.lblDateTime.text = otherDateString;
    }
    
    self.lblTitle.text = event.name;
    self.lblVenue.text = event.desc;
    
    NSString *bgImageURL = event.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgItem.image = image;
                                     });
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

@end
