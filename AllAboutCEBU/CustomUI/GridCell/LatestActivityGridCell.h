//
//  LatestActivityGridCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 23/1/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "GMGridViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface LatestActivityGridCell : GMGridViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblVenue;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
+ (LatestActivityGridCell*)gridCell;
-(void) bindDataOnGridCell:(NSDictionary *)data;
@end
