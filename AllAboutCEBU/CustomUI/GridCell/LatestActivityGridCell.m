//
//  LatestActivityGridCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 23/1/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LatestActivityGridCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"

@implementation LatestActivityGridCell

+ (LatestActivityGridCell*)gridCell {
    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:@"LatestActivityGridCell" owner:self options:nil];
    for (int i = 0; i < [bundle count]; i++) {
        if([[bundle objectAtIndex:i] class] == [self class]) {
            
            return [bundle objectAtIndex:i];
        }
    }
    return nil;
}

-(void) bindDataOnGridCell:(NSDictionary *)data {
    
    NSDictionary *filter = [data dictionaryByReplacingNullsWithBlanks];
    
    //NSLog(@"filter: %@", filter);
    
    if ([[filter objectForKey:@"DateTime"] length] == 0) {
        self.lblDateTime.text = @"No Date";
    } else {
        NSDate *dateinitial = [NSDate dateFromString:[data objectForKey:@"DateTime"]];
        NSString *otherDateString = [NSDate stringFromDate:dateinitial withFormat:@"EEE, dd MMM yyyy @ hh:mm aa"];
        self.lblDateTime.text = otherDateString;
    }
    NSString *bgImageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[filter objectForKey:@"ImgDashboard"]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:bgImageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    __weak typeof(self) weakSelf = self;
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     //dispatch_async(dispatch_get_main_queue(), ^{
                                         weakSelf.imgItem.image = image;
                                     //});
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     //NSLog(@"failed loading image: %@", error);
                                 }];

    
    self.lblTitle.text = [filter objectForKey:@"Title"];
    self.lblVenue.text = [filter objectForKey:@"Venue"];
    
}

@end
