//
//  LatestListingCollectionCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/3/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LatestListingCollectionCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"

@implementation LatestListingCollectionCell

-(void) bindDataOnGridCell:(Listing *)listing {
    NSString *imageURL = listing.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.imgItem.image = image;
                                     
                                     NSLog(@"loaded latestListings images");
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];
    
    
    self.lblTitle.text = listing.name;
    self.lblVenue.text = listing.address;
}

/*-(void) bindDataOnGridCell:(NSDictionary *)data {
    
    NSDictionary *filter = [data dictionaryByReplacingNullsWithBlanks];

    NSString *imageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[filter objectForKey:@"ImgDashboard"]];
    
    NSLog(@"dashboard image latest listings :%@",imageURL);
    
    //[self.imgItem setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.imgItem.image = image;
                                     
                                     NSLog(@"loaded latestListings images");
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];


    
    self.lblTitle.text = [filter objectForKey:@"Name"];
    self.lblVenue.text = [filter objectForKey:@"Address"];
}*/

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

@end
