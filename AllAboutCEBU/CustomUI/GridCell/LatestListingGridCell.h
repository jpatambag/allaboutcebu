//
//  LatestListingGridCell.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GMGridViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "Listing.h"

@interface LatestListingGridCell : GMGridViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgItem;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblVenue;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
+ (LatestListingGridCell*)gridCell;
-(void) bindDataOnGridCell:(Listing *)listing;
@end
