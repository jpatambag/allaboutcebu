//
//  LatestListingGridCell.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 8/2/15.
//  Copyright (c) 2015 Joseph Patambag. All rights reserved.
//

#import "LatestListingGridCell.h"
#import "NSDictionary+NullReplacement.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Listing.h"

@implementation LatestListingGridCell

+ (LatestListingGridCell*)gridCell {
    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:@"LatestListingGridCell" owner:self options:nil];
    for (int i = 0; i < [bundle count]; i++) {
        if([[bundle objectAtIndex:i] class] == [self class]) {
            
            return [bundle objectAtIndex:i];
        }
    }
    return nil;
}

/*-(void) bindDataOnGridCell:(NSDictionary *)data {
    
    NSDictionary *filter = [data dictionaryByReplacingNullsWithBlanks];
    
    NSString *imageURL = [NSString stringWithFormat:@"%@%@",ROOT_URL,[filter objectForKey:@"ImgDashboard"]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.imgItem.image = image;
                                     
                                     NSLog(@"loaded latestListings images");
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];
    
    //[self.imgItem setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    self.lblTitle.text = [filter objectForKey:@"Name"];
    self.lblVenue.text = [filter objectForKey:@"Address"];
    
}*/

-(void) bindDataOnGridCell:(Listing *)listing {
    NSString *imageURL = listing.dashboardImg;
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    __weak typeof(self) weakSelf = self;
    
    [self.imgItem setImageWithURLRequest:request
                        placeholderImage:[UIImage imageNamed:@"placeholder"]
                                 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                     });
                                     weakSelf.imgItem.image = image;
                                     
                                     NSLog(@"loaded latestListings images");
                                     
                                 } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                     NSLog(@"failed loading image: %@", error);
                                 }];
    
    
    self.lblTitle.text = listing.name;
    self.lblVenue.text = listing.address;
}

@end
