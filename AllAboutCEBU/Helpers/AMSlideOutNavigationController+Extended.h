//
//  AMSlideOutNavigationController+Extended.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/11/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "AMSlideOutNavigationController.h"
#import "RegistrationViewController.h"

@interface AMSlideOutNavigationController (Extended)
@property (nonatomic) RegistrationViewController *registrationController;
-(void) createHeaderForSideMenu:(NSDictionary *)object;
-(void) createCopywriteTxt;
-(void) callLogout;
-(void) bindDataOnView:(NSDictionary *)data;
@end
