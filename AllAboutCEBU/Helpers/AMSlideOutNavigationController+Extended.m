//
//  AMSlideOutNavigationController+Extended.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 15/11/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "AMSlideOutNavigationController+Extended.h"
#import "AFNetworking.h"
#import "NSDate+Helper.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "RootViewController.h"

@implementation AMSlideOutNavigationController (Extended)

-(void) createHeaderForSideMenu:(NSDictionary *)object {
    
    //NSLog(@"DATA FROM FACEBOOK :%@",object);
    
    //NSDictionary *fbData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kFBDATA];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/picture?",kFBGET_PROFILE_PIC,[object objectForKey:@"FbID"]]];
    
    //NSData *data = [NSData dataWithContentsOfURL:url];
    //UIImage *profilePic = [[[UIImage alloc] initWithData:data] autorelease];
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    header.backgroundColor = [UIColor clearColor];
    
    UIImageView *fbImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
    fbImage.contentMode = UIViewContentModeScaleAspectFit;
    [fbImage setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    [header addSubview:fbImage];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, self.view.frame.size.width - 20, 60 / 2)];
    nameLabel.text = [NSString stringWithFormat:@"%@ %@",[object objectForKey:@"FirstName"],[object objectForKey:@"Surname"]];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont fontWithName:@"Helvitica" size:16.0f];
    [header addSubview:nameLabel];
    
    self.tableView.tableHeaderView = header;
}

-(void) createCopywriteTxt {
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 60, self.view.frame.size.width, 60)];
    footer.backgroundColor = [UIColor clearColor];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, self.view.frame.size.width - 20, 60 / 2)];
    nameLabel.text = @" 2009 Yahaay Online Advertising Solutions";
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont fontWithName:@"Roboto-Light" size:11.0f];
    [footer addSubview:nameLabel];
    
    [self.tableView addSubview:footer];

}

-(void) bindDataOnView:(NSDictionary *)data {
    NSLog(@"AMSlideOutNavigationController is updated :%@",data);
}

-(void) callLogout {
    
    //if(!self.registrationController) {
    RegistrationViewController *controller  = [self.storyboard instantiateViewControllerWithIdentifier:@"registrationController"];
    //}
    
    
    
    UINavigationController *regNavigation = [[UINavigationController alloc] initWithRootViewController:controller];
    regNavigation.navigationBarHidden = YES;
    regNavigation.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    regNavigation.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self presentViewController:regNavigation
                       animated:YES
                     completion:nil];
    
    
}

@end
