//
//  Constants.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//


#define ERROR_DATA_REQUEST @"Server Error!"
#define kTEST_FLIGHT_TOKEN @"2b6c00d4-e0c8-4a8c-8484-610c8c2d8ba4"
#define ROOT_URL  @"https://www.allaboutcebu.com/" //@"http://dev.allaboutcebu.com/"
#define kREGISTERED @"REGISTERED"
#define kSHOWREGISTRATION_FORM @"SHOWREGISTRATION_FORM"

#define kGOOGLE_BANNER_ADD_UNIT @"ca-app-pub-0716842421671568/6014117754"
#define kGOOGLE_ANALYTICS_ID @"UA-63071413-1"

#define kGOOGLE_SERVICE_PLIST @"GoogleService-Info"

#define kHIDEREGISTRATION @"HIDEREGISTRATION"
#define kFACEBOOK_APP_ID @"119400871508987"
#define kFACEBOOK_TOKEN @"FACEBOOK_TOKEN"
#define kFBDATA @"FBDATAOBJECT"
#define kFBGET_PROFILE_PIC @"https://graph.facebook.com/"

#define kIPHONE_5 @"iPhone 5/5s"
#define kIPHONE_6 @"iPhone 6"
#define kIPHONE_6_PLUS @"iPhone 6 Plus"
#define kIPHONE_4_4S @"iPhone 4/4s"


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define APP_DELEGATE (AppDelegate*)[[UIApplication sharedApplication] delegate]

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IPHONE_4_4S (IS_IPHONE && SCREEN_MAX_LENGTH == 480.0)
#define IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE_6_PLUS (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

//API URL FOR DEVELOPMENT
#define kLISTING @"api/listing"
#define kHOTEL_LIST @"api/listing?name__PartialMatch=hotel"
#define kGETWEATHER_OF_THE_DAY @""
#define kGET_WEATHER_OF_WEEK @""
#define kGET_WEATHER @"/api/weather"
#define kLIST_OF_EVENTS @"api/event"
#define kIMAGE_URL @"api/image/"
#define kIMAGE_URL_PATH @"assets/Uploads/"
#define kGET_USER_INFO @"api/user/"
#define kCHECK_USER_EXIST @"api/user?email="
#define kGET_LATEST_EVENTS @"api/event/?DateTime__sort=DESC&__limit=5"
#define kGET_LATEST_LISTING @"api/listing"
#define kGET_LATEST_WEATHER @"?ID__sort=DESC&__limit=1"
#define kPLACEHOLDER_IMAGE @"assets/"
//#define kWEATHER_IMAGE_ICON @"http://openweathermap.org/img/w/"
#define kWEATHER_IMAGE_ICON @"assets/Uploads/forecast-io-icon/"
#define kGET_IMAGE @"getimage/list/"
#define kGET_WEATHER_BG_IMAGE @"getimage/weather"
#define kGET_WEATHER_FORECAST @"weather/forecast"
#define kGET_DASHBOARD_WEATHER @"getimage/weather/dashboard"
#define kGET_LIST_OF_CATEGORY @"api/ListCategory"
#define kGET_PAGING_FOR_LIST @"api/listing/?ID__sort=DESC&__limit[]=10&__limit[]=%@"
//#define kSEARCH_BY_CATEGORY @"api/listing/?Name__PartialMatch=%@&category=%@&__limit[]=%@"
#define kSEARCH_BY_CATEGORY @"api/listing/"
//#define KFORECAST_IO_URL @"https://api.forecast.io/forecast/b89f47b5d028d3517c795fc5d3e38de6/10.32,123.89?units=si"

#define KFORECAST_IO_URL @"weather/forecast"
#define KPER_PAGE 5
#define KSTART_PAGE 1

#define KURL_ENDPOINT @"http://www.allaboutcebu.com/mobile/user/"

/*
 Gets All Records
 http://dev.allaboutcebu.com/api/listing
 
 Gets a Record according to ID
 http://dev.allaboutcebu.com/api/listing/1
 
 Gets All Records with name has "hotel"
 http://dev.allaboutcebu.com/api/listing?name__PartialMatch
 =hotel

 
 Get weather data
 
 http://openweathermap.org/api
 
 http://www.wunderground.com/weather/api/
 
 */

//pagination - http://www.allaboutcebu.com/api/listing/?ID__sort=DESC&__limit[]=10&__limit[]=1
// search -http://www.allaboutcebu.com/api/listing/?Name__PartialMatch=BDO&category=bank&__limit[]=5&__limit[]=1


