//
//  FacebookObject.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/11/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@protocol FacebookObjectDelegate <NSObject>
@optional
-(void) didLoginWithFacebook:(NSString *)token;
-(void) didFailToRetrieveToken:(NSString *)msg;
-(void) didLogoutFacebook:(BOOL) isLogout;
-(void)didLoginTofacebook:(NSMutableDictionary *)object;
-(void)didLogoutTofacebook:(NSMutableDictionary *)object;
-(void)didGetFacebookUserInformation:(NSMutableDictionary *)object;
@end

@interface FacebookObject : NSObject
/*@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *facebookAccount;
@property (nonatomic, assign) int isFacebookAvailable;
@property (nonatomic, copy) NSString *globalmailID;
@property (nonatomic, copy) NSString *fbname;
@property (nonatomic, copy) NSDictionary *fbResponseObject;
@property (nonatomic, strong) id <FacebookObjectDelegate> delegate;
@property (nonatomic,strong) FBSession *fbSession;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;
- (void)requestUserInfo;
-(void)checkFacebook;
-(void)checkfacebookstatus;
-(void)get;
-(void) logoutFacebook;*/

@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) ACAccount *facebookAccount;
@property (nonatomic, assign) int isFacebookAvailable;
@property (nonatomic, copy) NSString *globalmailID;
@property (nonatomic, copy) NSString *fbname;
@property (nonatomic, copy) NSDictionary *fbResponseObject;
@property (nonatomic, strong) id <FacebookObjectDelegate> delegate;

-(void) loginToFacebook:(BOOL) isShare;
-(void) logoutUserFromFacebook;
-(void) requestPublishActionsPermission;
-(void) getFacebookUserInformation;
@end
