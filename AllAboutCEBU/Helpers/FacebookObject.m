//
//  FacebookObject.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/11/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "FacebookObject.h"
#import "Constants.h"

@interface FacebookObject()
@property (nonatomic,strong)  FBSDKLoginManager *loginManager;
@end

@implementation FacebookObject
/*- (void) checkUserSession {
    
}

// This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        //NSLog(@"Session opened with toke :%@",session.accessTokenData.accessToken);
        //NSLog(@"accessTokenData opened with toke :%@",session.accessTokenData);
        //NSLog(@"permissions opened with toke :%@",session.accessTokenData.permissions);
        
        if (_delegate && [_delegate respondsToSelector:@selector(didLoginWithFacebook:)]) {
            self.fbSession = session;
            [_delegate didLoginWithFacebook:session.accessTokenData.accessToken];
        }
        
        //[self requestUserInfo];
        
        // Show the user the logged-in UI
        //[self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        //NSLog(@"Session closed");
        
        if (_delegate && [_delegate respondsToSelector:@selector(didFailToRetrieveToken:)]) {
            self.fbSession = nil;
            [_delegate didFailToRetrieveToken:@"Authentication Failed!"];
        }
        
        
        // Show the user the logged-out UI
        //[self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        //NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                //NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
}

// Show the user the logged-out UI
- (void)userLoggedOut
{
    // Set the button title as "Log in with Facebook"
    //UIButton *loginButton = [self.customLoginViewController loginButton];
    //[loginButton setTitle:@"Log in with Facebook" forState:UIControlStateNormal];
    
    // Confirm logout message
    [self showMessage:@"You're now logged out Facebook" withTitle:@""];
}

// Show the user the logged-in UI
- (void)userLoggedIn
{
    // Set the button title as "Log out"
    //UIButton *loginButton = self.customLoginViewController.loginButton;
    //[loginButton setTitle:@"Log out" forState:UIControlStateNormal];
    
    // Welcome message
    [self showMessage:@"You're now logged in Facebook" withTitle:@"Welcome!"];
    
}

// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}

- (void)requestUserInfo {
    // We will request the user's public picture and the user's birthday
    // These are the permissions we need:
    NSArray *permissionsNeeded = @[@"user_birthday"];
    
    // Request the permissions the user currently has
    [FBRequestConnection startWithGraphPath:@"/me/permissions"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error){
                                  // These are the current permissions the user has
                                  NSDictionary *currentPermissions= [(NSArray *)[result data] objectAtIndex:0];
                                  
                                  // We will store here the missing permissions that we will have to request
                                  NSMutableArray *requestPermissions = [[NSMutableArray alloc] initWithArray:@[]];
                                  
                                  // Check if all the permissions we need are present in the user's current permissions
                                  // If they are not present add them to the permissions to be requested
                                  for (NSString *permission in permissionsNeeded){
                                      if (![currentPermissions objectForKey:permission]){
                                          [requestPermissions addObject:permission];
                                      }
                                  }
                                  
                                  // If we have permissions to request
                                  if ([requestPermissions count] > 0){
                                      // Ask for the missing permissions
                                      [FBSession.activeSession
                                       requestNewReadPermissions:requestPermissions
                                       completionHandler:^(FBSession *session, NSError *error) {
                                           if (!error) {
                                               // Permission granted, we can request the user information
                                               [self makeRequestForUserData];
                                           } else {
                                               // An error occurred, we need to handle the error
                                               // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
                                               NSLog(@"error %@", error.description);
                                           }
                                       }];
                                  } else {
                                      // Permissions are present
                                      // We can request the user information
                                      [self makeRequestForUserData];
                                  }
                                  
                              } else {
                                  // An error occurred, we need to handle the error
                                  // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
                                  NSLog(@"error %@", error.description);
                              }
                          }];
}

- (void) makeRequestForUserData
{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // Success! Include your code to handle the results here
            //NSLog(@"user info: %@", result);
            
            
            
        } else {
            // An error occurred, we need to handle the error
            // Check out our error handling guide: https://developers.facebook.com/docs/ios/errors/
            //NSLog(@"error %@", error.description);
        }
    }];
}

-(void)checkFacebook
{
    self.accountStore = [[ACAccountStore alloc]init];
    ACAccountType *FBaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSArray *fbPermission = @[
                              @"user_birthday",
                              ];
    
    NSString *key = @"489180221137539";
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,fbPermission,ACFacebookPermissionsKey, nil];
    //NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"f026758a9bd44a3949d5c9c31b581a84", ACFacebookAppIdKey, fbPermission, ACFacebookPermissionsKey, ACFacebookAudienceOnlyMe, ACFacebookAudienceKey, nil];
    
    [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *e) {
         if (granted)
         {
             NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
             //it will always be the last object with single sign on
             self.facebookAccount = [accounts lastObject];
             
             
             ACAccountCredential *facebookCredential = [self.facebookAccount credential];
             NSString *accessToken = [facebookCredential oauthToken];
             //NSLog(@"Facebook Access Token: %@", accessToken);
             
             //NSLog(@"facebook account =%@",self.facebookAccount);
             
             [self get];
             
             //[self getFBFriends];
             
             _isFacebookAvailable = 1;
         } else
         {
             //Fail gracefully...
             //NSLog(@"error getting permission :%@ %@ %@ %@",e.debugDescription,e.domain, e.description,e.userInfo);
             
             _isFacebookAvailable = 0;
             
         }
     }];
 
}

-(void)checkfacebookstatus
{
    if (_isFacebookAvailable == 0)
    {
        [self checkFacebook];
        _isFacebookAvailable = 1;
    }
    else
    {
        printf("Get out from our game");
    }
}


-(void)get
{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
    request.account = self.facebookAccount;
    
    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        
        if(!error)
        {
            
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            //NSLog(@"Dictionary contains: %@", list );
            
            
            
            
            self.globalmailID   = [NSString stringWithFormat:@"%@",[list objectForKey:@"email"]];
            //NSLog(@"global mail ID : %@",self.globalmailID);
            
            
            self.fbname = [NSString stringWithFormat:@"%@",[list objectForKey:@"name"]];
            //NSLog(@"faceboooookkkk name %@",self.fbname);
            
            
            
            
            if([list objectForKey:@"error"]!=nil)
            {
                [self attemptRenewCredentials];
            }
            dispatch_async(dispatch_get_main_queue(),^{
                
            });
        }
        else
        {
            //handle error gracefully
            //NSLog(@"error from get%@",error);
            //attempt to revalidate credentials
        }
        
    }];
    
    self.accountStore = [[ACAccountStore alloc]init];
    ACAccountType *FBaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSString *key = @"451805654875339";
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,@[@"friends_videos"],ACFacebookPermissionsKey, nil];
    
    
    [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *e) {}];
    
}




-(void)accountChanged:(NSNotification *)notification
{
    [self attemptRenewCredentials];
}

-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    //NSLog(@"Good to go");
                    [self get];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    //NSLog(@"User declined permission");
                    break;
                case ACAccountCredentialRenewResultFailed:
                    //NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error gracefully
            //NSLog(@"error from renew credentials%@",error);
        }
    }];
}

-(void) logoutFacebook {
    [FBSession.activeSession closeAndClearTokenInformation];
    if (_delegate && [_delegate respondsToSelector:@selector(didLogoutFacebook:)]) {
        [_delegate didLogoutFacebook:YES];
    }
} */

-(void) loginToFacebook:(BOOL) isShare {
    
    
    __block NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    if (!self.loginManager) {
        self.loginManager = [[FBSDKLoginManager alloc] init];
    }
    
    [self.loginManager logInWithReadPermissions:@[@"public_profile",@"email",@"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            [params setObject:@"" forKey:@"fbtoken"];
            [params setObject:[NSNumber numberWithBool:0] forKey:@"isLogin"];
            [params setObject:error.description forKey:@"errorDesc"];
            
            //NSLog(@"error login");
            
            if (_delegate && [_delegate respondsToSelector:@selector(didLoginTofacebook:)]) {
                [_delegate didLoginTofacebook:params];
            }
            
        } else if (result.isCancelled) {
            // Handle cancellations
            
            [params setObject:@"" forKey:@"fbtoken"];
            [params setObject:[NSNumber numberWithBool:0] forKey:@"isLogin"];
            [params setObject:@"User Cancelled" forKey:@"errorDesc"];
            
            //NSLog(@"User Cancelled");
            
            if (_delegate && [_delegate respondsToSelector:@selector(didLoginTofacebook:)]) {
                [_delegate didLoginTofacebook:params];
            }
            
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            
            //NSLog(@"token is :%@",result.token);
            //NSLog(@"grantedPermissions :%@",result.grantedPermissions);
            //NSLog(@"grantedPermissions :%@",result.declinedPermissions);
            
            if ([result.grantedPermissions containsObject:@"public_profile"]) {
                [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
                [params setObject:[FBSDKAccessToken currentAccessToken] forKey:@"fbtoken"];
                [params setObject:[NSNumber numberWithBool:1] forKey:@"isLogin"];
                [params setObject:@"" forKey:@"errorDesc"];
                [params setObject:[NSNumber numberWithBool:1] forKey:@"didlogintofb"];
                
                if (isShare) {
                    [params setObject:[NSNumber numberWithBool:1] forKey:@"shareAfterLogin"];
                }
                
                
                //[self requestPublishActionsPermission];
                
                if (_delegate && [_delegate respondsToSelector:@selector(didLoginTofacebook:)]) {
                    [_delegate didLoginTofacebook:params];
                }
                
            }
            
        }
        
    }];
    
}

-(void) requestPublishActionsPermission {
    __block NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

    FBSDKLoginManager *loginManager2 = [[FBSDKLoginManager alloc] init];
    
    [loginManager2
     logInWithPublishPermissions:@[@"publish_actions"]
     handler:^(FBSDKLoginManagerLoginResult *loginResult, NSError *error) {
         
         NSLog(@"result.grantedPermissions for publish_actions :%@",loginResult.grantedPermissions);
         
         if ([loginResult.grantedPermissions containsObject:@"publish_actions"]) {
             // TODO: publish content.
             
             //NSLog(@"token is :%@",loginResult.token);
             //NSLog(@"grantedPermissions for publish_actions :%@",loginResult.grantedPermissions);
             //NSLog(@"grantedPermissions for publish_actions :%@",loginResult.declinedPermissions);
             
             [params setObject:[FBSDKAccessToken currentAccessToken] forKey:@"fbtoken"];
             [params setObject:[NSNumber numberWithBool:1] forKey:@"isLogin"];
             [params setObject:@"" forKey:@"errorDesc"];
             [params setObject:[NSNumber numberWithBool:1] forKey:@"didlogintofb"];
             [params setObject:[NSNumber numberWithBool:1] forKey:@"shareAfterLogin"];
             [params setObject:[NSNumber numberWithBool:1] forKey:@"hasPublisPermission"];
             //[self requestPublishActionsPermission];
             
             if (_delegate && [_delegate respondsToSelector:@selector(didLoginTofacebook:)]) {
                 [_delegate didLoginTofacebook:params];
             }
             
         } else {
             // This would be a nice place to tell the user why publishing
             // is valuable.
             
             NSLog(@"ERROR GETTING PUBLISH PERSMISSIOn :%@", error.description);
         }
     }];
    
}

-(void) getFacebookUserInformation {
    __block NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 //NSLog(@"fetched user:%@", result);
                 
                 [params setObject:[FBSDKAccessToken currentAccessToken] forKey:@"fbtoken"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"isLogin"];
                 [params setObject:@"Unable to retrieve user information" forKey:@"errorDesc"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"didlogintofb"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"shareAfterLogin"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"hasPublisPermission"];
                 [params setObject:result forKey:@"userFacebookData"];
                 //[self requestPublishActionsPermission];
                 
             } else {
                 [params setObject:[FBSDKAccessToken currentAccessToken] forKey:@"fbtoken"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"isLogin"];
                 [params setObject:@"" forKey:@"errorDesc"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"didlogintofb"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"shareAfterLogin"];
                 [params setObject:[NSNumber numberWithBool:1] forKey:@"hasPublisPermission"];

             }
             
             if (_delegate && [_delegate respondsToSelector:@selector(didGetFacebookUserInformation:)]) {
                 self.fbResponseObject = result;
                 [_delegate didGetFacebookUserInformation:params];
             }
             
         }];
    }
}

-(void) logoutUserFromFacebook {
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    
    [self.loginManager logOut];

    if (_delegate && [_delegate respondsToSelector:@selector(didLogoutFacebook:)]) {
        [_delegate didLogoutFacebook:YES];
    }
}


@end
