//
//  Global.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define GlobalInstance [Global sharedInstance]

@interface Global : NSObject
@property (nonatomic,assign) BOOL viewFromDashboard;
@property (nonatomic, strong) NSString *latestEndpointURL;
@property (nonatomic, strong) NSString *profileEndpointURL;
+(Global *)sharedInstance;
- (void)showAlert:(NSString *)title message:(NSString *)message;
- (NSArray *)loadPlistFile:(NSString *)filename;
- (NSDictionary *)loadPlistFileDictionary:(NSString *)filename;
- (NSArray *)loadArrayPlistFile:(NSString *)filename;
- (NSString *)getPlistPath:(NSString *)filename;
- (NSArray *)loadPlistfile:(NSString *)name forKey:(NSString *)key;

- (BOOL) defaultExistWithName:(NSString *)str;
- (BOOL) registerDefaultWithKey:(NSString *) key andValue:(NSString *)val;
- (NSString *)checkScreen;
- (NSArray *)getFireBaseProviders;
@end
