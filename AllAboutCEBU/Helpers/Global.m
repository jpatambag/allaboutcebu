//
//  Global.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "Global.h"
#import "Constants.h"
#import <FirebaseAuthUI/FirebaseAuthUI.h>
#import <FirebaseFacebookAuthUI/FUIFacebookAuth.h>
#import <FirebaseGoogleAuthUI/FUIGoogleAuth.h>
#import <FirebaseTwitterAuthUI/FUITwitterAuth.h>
#import "FUICustomAuthPickerViewController.h"
#import <FirebaseAuth/FirebaseAuth.h>


@implementation Global

+ (Global*)sharedInstance
{
    static Global *sharedInstance;
    static dispatch_once_t done;
    dispatch_once(&done, ^{
        sharedInstance = [[Global alloc] init]; });
    return sharedInstance;
}

- (void)showAlert:(NSString *)title message:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

// -------------------------------------------------------------------------
// Load Plist File
// -------------------------------------------------------------------------
- (NSArray *)loadPlistFile:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray *aList = [dict objectForKey:@"Root"];
    
    return aList;
}

- (NSDictionary *)loadPlistFileDictionary:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    return dict;
}

// -------------------------------------------------------------------------
// Load Array Plist File
// -------------------------------------------------------------------------
- (NSArray *)loadArrayPlistFile:(NSString *)filename
{
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    NSArray *aList = [[NSArray alloc] initWithContentsOfFile:path];
    
    return aList;
}

// -------------------------------------------------------------------------
// Retrieves the plist path
// -------------------------------------------------------------------------
- (NSString *)getPlistPath:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    NSString *path = [cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",filename]];
    
    return path;
}

// -------------------------------------------------------------------------
// Check if the key exist
// -------------------------------------------------------------------------
- (BOOL) defaultExistWithName:(NSString *)str {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:str])
        return YES;
    else
        return NO;
    
}

// -------------------------------------------------------------------------
// Register defaults with key
// -------------------------------------------------------------------------
- (BOOL) registerDefaultWithKey:(NSString *) key andValue:(NSString *)val {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    [defaults setObject:key forKey:val];
    
    if ([defaults synchronize]) {
        return YES;
    } else {
        return NO;
    }
    
}

- (NSArray *)loadPlistfile:(NSString *)name forKey:(NSString *)key {
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray *arraylist = [dict objectForKey:key];
    
    return arraylist;
}

-(NSString *)checkScreen {
    
    NSString *screen = @"";
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if( screenHeight < screenWidth ){
        screenHeight = screenWidth;
    }
    
    if( screenHeight > 480 && screenHeight < 667 ){
        screen = kIPHONE_5;
    } else if ( screenHeight > 480 && screenHeight < 736 ){
        screen = kIPHONE_6;
    } else if ( screenHeight > 480 ){
        screen = kIPHONE_6_PLUS;
    } else {
        screen = kIPHONE_4_4S;
    }
    
    return screen;
}

- (NSArray *)getFireBaseProviders {
    NSMutableArray *providers = [NSMutableArray new];
    id<FUIAuthProvider> emailProvider;
    //id<FUIAuthProvider> googleProvider;
    //id<FUIAuthProvider> facebookProvider;
    
    emailProvider =  [[FUIGoogleAuth alloc] initWithScopes:@[kGoogleUserInfoEmailScope,
                                                             kGoogleUserInfoProfileScope,
                                                             kGoogleGamesScope,
                                                             kGooglePlusMeScope]];
    //facebookProvider = [[FUIFacebookAuth alloc] initWithPermissions:<#(nonnull NSArray *)#>];
    //googleProvider =  [[FUIGoogleAuth alloc] init];
    [providers addObject:emailProvider];
    
    return providers;
}

@end
