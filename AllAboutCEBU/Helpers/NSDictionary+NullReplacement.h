//
//  NSDictionary+NullReplacement.h
//  IPrecheck
//
//  Created by Joseph Patambag on 1/5/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NullReplacement)
- (NSDictionary *)dictionaryByReplacingNullsWithBlanks;
@end
