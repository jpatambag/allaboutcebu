//
//  NetworkLayer.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>
#define NetworkInstance [NetworkLayer networkInstance]

@protocol NetworkLayerDelegate <NSObject>
@optional
-(void) serverResponse:(id) response tag:(int)apiTag;
-(void) serverResponseWithDataObject:(id) response tag:(int)apiTag object:(id)theObject;
@end

@interface NetworkLayer : NSObject
@property (nonatomic, strong) id <NetworkLayerDelegate> delegate;

+(NetworkLayer *)networkInstance;
-(void) requestWeatherData:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag;
-(void) requestDataServerWithDataObject:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag withObject:(id) object;
-(void) requestDataFromServer:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag;
-(void) requestPOSTDataToServer:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag;
-(void) responseReceived:(id) response tag:(int)apiTag;
@end
