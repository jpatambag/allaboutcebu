//
//  NetworkLayer.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 17/10/14.
//  Copyright (c) 2014 Joseph Patambag. All rights reserved.
//

#import "NetworkLayer.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "Global.h"

@implementation NetworkLayer

+(NetworkLayer *)networkInstance {
    static NetworkLayer *network;
    static dispatch_once_t done;
    dispatch_once(&done, ^{
        network = [[NetworkLayer alloc] init];
    });
    return network;
}


-(void) requestDataFromServer:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag{
   
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", ROOT_URL, url];
    
    
    /*AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //NSString *urlString = [NSString stringWithFormat:@"%@", @"http://www.allaboutcebu.com/api/listing"];
    NSLog(@"REQUEST URL FOR TAG :%d : %@",tag, urlString);
    
    NSURL *urlRequest = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlRequest];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    //operation.securityPolicy.allowInvalidCertificates = YES;
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setAllowInvalidCertificates:YES];
    operation.securityPolicy = securityPolicy;
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"RESPONSE OBJECT IN ARRAY :%@",responseObject);
        
        [self responseReceived:responseObject tag:tag];
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
    }]; */
    
    
    //NSURL *URL = [NSURL URLWithString:urlString];
    //NSURLRequest *request = [NSURLRequest requestWithURL:URL];
 
    /*AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager GET:urlString parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self responseReceived:responseObject tag:tag];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
    }];*/
    
    /*NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
        } else {
            [self responseReceived:responseObject tag:tag];
        }
    }];
    [dataTask resume];*/
    
    NSURL *URL = [NSURL URLWithString:urlString];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URL.absoluteString parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self responseReceived:responseObject tag:tag];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void) requestWeatherData:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", ROOT_URL, url];
    //NSLog(@"requestWeatherData URL FOR TAG :%d : %@",tag, urlString);
    /*AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSURL *urlRequest = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlRequest];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    //operation.securityPolicy.allowInvalidCertificates = YES;
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setAllowInvalidCertificates:YES];
    operation.securityPolicy = securityPolicy;
    
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"RESPONSE OBJECT IN ARRAY :%@",responseObject);
        
        [self responseReceived:responseObject tag:tag];
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
    }];*/
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
        } else {
            [self responseReceived:responseObject tag:tag];
        }
    }];
    [dataTask resume];
}

-(void) requestDataServerWithDataObject:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag withObject:(id) object {
    NSString *urlString = [NSString stringWithFormat:@"%@%@", ROOT_URL, url];
    /*AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //NSLog(@"REQUEST URL FOR TAG :%d : %@",tag, urlString);
    
    NSURL *urlRequest = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlRequest];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    //operation.securityPolicy.allowInvalidCertificates = YES;
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setAllowInvalidCertificates:YES];
    operation.securityPolicy = securityPolicy;

    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSLog(@"RESPONSE OBJECT IN ARRAY :%@",responseObject);
        
        [self responseReceived:responseObject tag:tag];
    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
        [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
    }];*/
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
        } else {
            [self responseReceived:responseObject tag:tag];
        }
    }];
    [dataTask resume];
    
}

-(void) requestPOSTDataToServer:(NSString *)url andParams:(NSDictionary *)params forTag:(int) tag {
    /*AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    NSString *urlString = [NSString stringWithFormat:@"%@%@", ROOT_URL, url];
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //manager.securityPolicy.allowInvalidCertificates = YES;
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    [securityPolicy setAllowInvalidCertificates:NO];
    manager.securityPolicy = securityPolicy;
    
    manager.requestSerializer = requestSerializer;;
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //NSLog(@"JSON RESPONSE: %@", responseObject);
        
        //[self responseReceived:responseObject tag:tag];
    
        if ([responseObject count] > 0) {
            
            [self responseReceived:responseObject tag:tag];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
       [GlobalInstance showAlert:ERROR_DATA_REQUEST message:[error localizedDescription]];
    }];*/
    
}

-(void) responseReceived:(id) response tag:(int)apiTag; {
    [_delegate serverResponse:response tag:apiTag];
}

-(void) serverResponseWithDataObject:(id) response tag:(int)apiTag object:(id)theObject {
    [_delegate serverResponseWithDataObject:response tag:apiTag object:theObject];
}

@end
