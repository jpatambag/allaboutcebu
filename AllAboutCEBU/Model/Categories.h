//
//  Categories.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 14/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Categories : NSObject
@property (nonatomic, copy) NSString *categoryImg;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *uid;
+ (Categories *)categoryWithObject:(NSDictionary *)data;
@end
