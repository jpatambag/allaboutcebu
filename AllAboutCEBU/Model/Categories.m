//
//  Categories.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 14/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "Categories.h"

@implementation Categories
+ (Categories *)categoryWithObject:(NSDictionary *)data {
    Categories *object = [[self alloc] init];
    
    if ([data objectForKey:@"categoryImg"]) {
        object.categoryImg = [data objectForKey:@"categoryImg"];
    }
    
    if ([data objectForKey:@"desc"]) {
        object.desc = [data objectForKey:@"desc"];
    }
    
    if ([data objectForKey:@"name"]) {
        object.name = [data objectForKey:@"name"];
    }
    
    if ([data objectForKey:@"uid"]) {
        object.uid = [data objectForKey:@"uid"];
    }
    
    return object;
}
@end
