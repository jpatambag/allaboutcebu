//
//  CategoryItems.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryItems : NSObject

@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *dashboardImg;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *listingImg;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *urlSegment;

+ (CategoryItems *)itemWithObject:(NSDictionary *)data;
+ (instancetype)place;
@end
