//
//  CategoryItems.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "CategoryItems.h"

@implementation CategoryItems
+ (CategoryItems *)itemWithObject:(NSDictionary *)data {
    CategoryItems *object = [[self alloc] init];
    
    if ([data objectForKey:@"address"]) {
        object.address = [data objectForKey:@"address"];
    }
    
    if ([data objectForKey:@"author"]) {
        object.author = [data objectForKey:@"author"];
    }
    
    if ([data objectForKey:@"category"]) {
        object.category = [data objectForKey:@"category"];
    }
    
    if ([data objectForKey:@"dashboardImg"]) {
        object.dashboardImg = [data objectForKey:@"dashboardImg"];
    }
    
    if ([data objectForKey:@"lat"]) {
        object.lat = [data objectForKey:@"lat"];
    }
    
    if ([data objectForKey:@"listingImg"]) {
        object.listingImg = [data objectForKey:@"listingImg"];
    }
    
    if ([data objectForKey:@"lng"]) {
        object.lng = [data objectForKey:@"lng"];
    }
    
    if ([data objectForKey:@"name"]) {
        object.name = [data objectForKey:@"name"];
    }
    
    if ([data objectForKey:@"uid"]) {
        object.uid = [data objectForKey:@"uid"];
    }
    
    if ([data objectForKey:@"urlSegment"]) {
        object.urlSegment = [data objectForKey:@"urlSegment"];
    }
    
    return object;

}

+ (instancetype)place {
    CategoryItems *place = [[CategoryItems alloc] init];
    return place;
}

@end
