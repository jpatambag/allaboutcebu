//
//  Events.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Events : NSObject
@property (nonatomic,strong) NSDictionary *object;
@property (nonatomic,copy) NSString *author;
@property (nonatomic,copy) NSString *dashboardImg;
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *listingImg;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSNumber *starCount;
@property (nonatomic,copy) NSNumber *uid;
@property (nonatomic,copy) NSString *lat;
@property (nonatomic,copy) NSString *lng;
@property (nonatomic,copy) NSString *urlSegment;
@property (nonatomic,copy) NSString *venue;

+ (Events *)eventWithObject:(NSDictionary *)data;
@end
