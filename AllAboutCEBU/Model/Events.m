//
//  Events.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "Events.h"

@implementation Events

+ (Events *)eventWithObject:(NSDictionary *)data {
    Events *object = [[self alloc] init];
    
    if ([data objectForKey:@"author"]) {
        object.author = [data objectForKey:@"author"];
    }
    
    if ([data objectForKey:@"dashboardImg"]) {
        object.dashboardImg = [data objectForKey:@"dashboardImg"];
    }
    
    if ([data objectForKey:@"date"]) {
        object.date = [data objectForKey:@"date"];
    }
    
    if ([data objectForKey:@"desc"]) {
        object.desc = [data objectForKey:@"desc"];
    }
    
    if ([data objectForKey:@"listingImg"]) {
        object.listingImg = [data objectForKey:@"listingImg"];
    }
    
    if ([data objectForKey:@"name"]) {
        object.name = [data objectForKey:@"name"];
    }
    
    if ([data objectForKey:@"starCount"]) {
        object.starCount = [NSNumber numberWithInt: [[data objectForKey:@"starCount"] intValue]];
    }
    
    if ([data objectForKey:@"uid"]) {
        object.uid = [data objectForKey:@"uid"];
    }
    
    if ([data objectForKey:@"lat"]) {
        object.lat = [data objectForKey:@"lat"];
    }
    
    if ([data objectForKey:@"lng"]) {
        object.lng = [data objectForKey:@"lng"];
    }
    
    if ([data objectForKey:@"urlSegment"]) {
        object.urlSegment = [data objectForKey:@"urlSegment"];
    }

    if ([data objectForKey:@"venue"]) {
        object.venue = [data objectForKey:@"venue"];
    }

    
    return object;
}
@end
