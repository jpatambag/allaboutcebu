//
//  Places.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/12/15.
//  Copyright © 2015 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Places : NSObject
@property (nonatomic,copy) NSNumber *ID;
@property (nonatomic,copy) NSString *Name;
@property (nonatomic,copy) NSNumber *Status;
@property (nonatomic,copy) NSString *Address;
@property (nonatomic,copy) NSString *PhoneNumber;
@property (nonatomic,copy) NSString *Subscription;
@property (nonatomic,copy) NSString *Category;
@property (nonatomic,copy) NSNumber *lat;
@property (nonatomic,copy) NSNumber *lng;
@property (nonatomic,copy) NSString *ImgDashboard;
@property (nonatomic,copy) NSString *ImgListView;
@property (nonatomic,copy) NSString *ImgListDetail;
@property (nonatomic,copy) NSNumber *Popularity;
@property (nonatomic,copy) NSNumber *User;

+ (Places *)placesWithObject:(NSDictionary *)data;
@end
