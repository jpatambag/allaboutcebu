//
//  Places.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 20/12/15.
//  Copyright © 2015 Joseph Patambag. All rights reserved.
//

#import "Places.h"

@implementation Places

+ (Places *)placesWithObject:(NSDictionary *)data {
    Places *object = [[self alloc] init];
    
    if ([data objectForKey:@"ID"]) {
        object.ID = [data objectForKey:@"ID"];
    }

    if ([data objectForKey:@"Name"]) {
        object.Name = [data objectForKey:@"Name"];
    }
    if ([data objectForKey:@"Status"]) {
        object.Status = [data objectForKey:@"Status"];
    }
    if ([data objectForKey:@"Address"]) {
        object.Address = [data objectForKey:@"Address"];
    }
    if ([data objectForKey:@"PhoneNumber"]) {
        object.PhoneNumber = [data objectForKey:@"PhoneNumber"];
    }
    if ([data objectForKey:@"Subscription"]) {
        object.Subscription = [data objectForKey:@"Subscription"];
    }
    
    if ([data objectForKey:@"Category"]) {
        object.Category = [data objectForKey:@"Category"];
    }
    
    if ([data objectForKey:@"lat"]) {
        object.lat = [data objectForKey:@"lat"];
    }
    
    if ([data objectForKey:@"lng"]) {
        object.lng = [data objectForKey:@"lng"];
    }
    
    if ([data objectForKey:@"ImgDashboard"]) {
        object.ImgDashboard = [data objectForKey:@"ImgDashboard"];
    }
    
    if ([data objectForKey:@"ImgListView"]) {
        object.ImgListView = [data objectForKey:@"ImgListView"];
    }
    
    if ([data objectForKey:@"ImgListDetail"]) {
        object.ImgListDetail = [data objectForKey:@"ImgListDetail"];
    }
    
    if ([data objectForKey:@"Popularity"]) {
        object.Popularity = [data objectForKey:@"Popularity"];
    }
    
    if ([data objectForKey:@"User"]) {
        object.User = [data objectForKey:@"User"];
    }
    
    return object;
}
@end


