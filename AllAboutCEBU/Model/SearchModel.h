//
//  SearchModel.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 12/2/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 
 Address = "Salinas Drive Lahug, Cebu City";
 Categories =     (
 1
 );
 Category = hotel;
 ID = 17;
 ImageGallery = 60;
 ImgDashboard = "assets/Uploads/_resampled/SetRatioSize414190-Cebu-City.png";
 ImgListDetail = "assets/Uploads/_resampled/SetRatioSize414190-Cebu-City.png";
 ImgListView = "assets/Uploads/_resampled/SetRatioSize330270-Cebu-City.png";
 ImgWebListThumb = "assets/Uploads/_resampled/SetRatioSize414190-Cebu-City.png";
 Name = "Waterfront Cebu City Casino Hotel - Lahug";
 PhoneNumber = "+63322326888";
 Popularity = 2;
 Status = 0;
 Subscription = Free;
 Tags = "<null>";
 URLSegment = "waterfront-cebu-city-casino-hotel-lahug";
 User = 3;
 lat = "10.325236617997714";
 lng = "123.90481620705418";
 
 */


@interface SearchModel : NSObject
@property (nonatomic, assign) int ID;
@property (nonatomic, copy) NSString *Name;
@property (nonatomic, copy) NSString *Status;
@property (nonatomic, copy) NSString *URLSegment;
@property (nonatomic, copy) NSString *Address;
@property (nonatomic, copy) NSString *PhoneNumber;
@property (nonatomic, copy) NSString *Subscription;
@property (nonatomic, copy) NSString *Category;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *ImgDashboard;
@property (nonatomic, copy) NSString *ImgListView;
@property (nonatomic, copy) NSString *ImgListDetail;
@property (nonatomic, copy) NSString *Popularity;
@property (nonatomic, assign) int User;
@property (nonatomic, strong) NSArray *Categories;
@property (nonatomic, assign) int ImageGallery;
@property (nonatomic, copy) NSString * ImgWebListThumb;
@property (nonatomic, copy) NSString *Tags;

//+ (instancetype)placesWithModel:(SearchModel *) model;
+ (instancetype)place;
//- (instancetype)initWithPlace:(SearchModel *)model;

@end