//
//  Weather.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Weather : NSObject
@property (nonatomic,strong) NSDictionary *object;
@property (nonatomic, strong) NSArray *forecast;
@property (nonatomic, copy) NSDecimalNumber *apparentTemperature;
@property (nonatomic, copy) NSDecimalNumber *cloudCover;
@property (nonatomic, copy) NSDecimalNumber *dewPoint;
@property (nonatomic, copy) NSDecimalNumber *humidity;
@property (nonatomic, copy) NSString *dashboardIcon;
@property (nonatomic, copy) NSString *dashboardImg;
@property (nonatomic, copy) NSDecimalNumber *ozone;
@property (nonatomic, copy) NSDecimalNumber *precipIntensity;
@property (nonatomic, copy) NSDecimalNumber *precipProbability;
@property (nonatomic, copy) NSString *precipType;
@property (nonatomic, copy) NSDecimalNumber *pressure;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *sunriseTime;
@property (nonatomic, copy) NSString *sunsetTime;
@property (nonatomic, copy) NSDecimalNumber *temperature;
@property (nonatomic, copy) NSNumber *time;
@property (nonatomic, copy) NSDecimalNumber *windBearing;
@property (nonatomic, copy) NSDecimalNumber *windSpeed;
+ (Weather *)weatherWithObject:(NSDictionary *)data;
@end
