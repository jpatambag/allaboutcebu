//
//  Weather.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 6/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "Weather.h"

@implementation Weather
+ (Weather *)weatherWithObject:(NSDictionary *)data {
    Weather *object = [[self alloc] init];
    
    if ([data objectForKey:@"_forecast"]) {
        object.forecast = [data objectForKey:@"_forecast"];
    }
    
    if ([data objectForKey:@"apparentTemperature"]) {
        object.apparentTemperature = [data objectForKey:@"apparentTemperature"];
    }
    
    if ([data objectForKey:@"dewPoint"]) {
        object.dewPoint = [data objectForKey:@"dewPoint"];
    }
    
    if ([data objectForKey:@"humidity"]) {
        object.humidity = [data objectForKey:@"humidity"];
    }
    
    if ([data objectForKey:@"dashboardIcon"]) {
        object.dashboardIcon = [data objectForKey:@"dashboardIcon"];
    }
    
    if ([data objectForKey:@"dashboardImg"]) {
        object.dashboardImg = [data objectForKey:@"dashboardImg"];
    }
    
    if ([data objectForKey:@"ozone"]) {
        object.ozone = [data objectForKey:@"ozone"];
    }
    
    if ([data objectForKey:@"precipIntensity"]) {
        object.precipIntensity = [data objectForKey:@"precipIntensity"];
    }
    
    if ([data objectForKey:@"precipProbability"]) {
        object.precipProbability = [data objectForKey:@"precipProbability"];
    }
    
    if ([data objectForKey:@"precipType"]) {
        object.precipType = [data objectForKey:@"precipType"];
    }
    
    if ([data objectForKey:@"pressure"]) {
        object.pressure = [data objectForKey:@"pressure"];
    }
    
    if ([data objectForKey:@"summary"]) {
        object.summary = [data objectForKey:@"summary"];
    }
    
    if ([data objectForKey:@"sunriseTime"]) {
        object.sunriseTime = [data objectForKey:@"sunriseTime"];
    }
    
    if ([data objectForKey:@"sunsetTime"]) {
        object.sunsetTime = [data objectForKey:@"sunsetTime"];
    }
    
    if ([data objectForKey:@"temperature"]) {
        object.temperature = [data objectForKey:@"temperature"];
    }
    
    if ([data objectForKey:@"time"]) {
        object.time = [data objectForKey:@"time"];
    }
    
    if ([data objectForKey:@"windBearing"]) {
        object.windBearing = [data objectForKey:@"windBearing"];
    }
    
    if ([data objectForKey:@"windSpeed"]) {
        object.windSpeed = [data objectForKey:@"windSpeed"];
    }
    
    return object;

}
@end
