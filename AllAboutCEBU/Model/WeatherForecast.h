//
//  WeatherForecast.h
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherForecast : NSObject
@property (nonatomic, copy) NSDecimalNumber *apparentTemperature;
@property (nonatomic, copy) NSDecimalNumber *cloudCover;
@property (nonatomic, copy) NSDecimalNumber *dewPoint;
@property (nonatomic, copy) NSDecimalNumber *humidity;
@property (nonatomic, copy) NSString *dashboardIcon;
@property (nonatomic, copy) NSString *dashboardImg;
@property (nonatomic, copy) NSString *iconImg;
@property (nonatomic, copy) NSDecimalNumber *ozone;
@property (nonatomic, copy) NSDecimalNumber *precipIntensity;
@property (nonatomic, copy) NSDecimalNumber *precipProbability;
@property (nonatomic, copy) NSString *precipType;
@property (nonatomic, copy) NSDecimalNumber *pressure;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *sunriseTime;
@property (nonatomic, copy) NSString *sunsetTime;
@property (nonatomic, copy) NSDecimalNumber *temperature;
@property (nonatomic, copy) NSNumber *time;
@property (nonatomic, copy) NSDecimalNumber *windBearing;
@property (nonatomic, copy) NSDecimalNumber *windSpeed;
@property (nonatomic, copy) NSDecimalNumber *apparentTemperatureMax;
@property (nonatomic, copy) NSNumber *apparentTemperatureMaxTime;
@property (nonatomic, copy) NSDecimalNumber *apparentTemperatureMin;
@property (nonatomic, copy) NSNumber *apparentTemperatureMinTime;
@property (nonatomic, copy) NSDecimalNumber *moonPhase;
@property (nonatomic, copy) NSDecimalNumber *precipIntensityMax;
@property (nonatomic, copy) NSNumber *precipIntensityMaxTime;
@property (nonatomic, copy) NSNumber *temperatureMinTime;
@property (nonatomic, copy) NSDecimalNumber *temperatureMax;
@property (nonatomic, copy) NSDecimalNumber *temperatureMaxTime;
@property (nonatomic, copy) NSDecimalNumber *temperatureMin;

+ (WeatherForecast *)forecastWithData:(NSDictionary *)data;
@end
