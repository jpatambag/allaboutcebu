//
//  WeatherForecast.m
//  AllAboutCEBU
//
//  Created by Joseph Patambag on 11/7/16.
//  Copyright © 2016 Joseph Patambag. All rights reserved.
//

#import "WeatherForecast.h"

@implementation WeatherForecast

+ (WeatherForecast *)forecastWithData:(NSDictionary *)data {
    WeatherForecast *object = [[self alloc] init];
    
    if ([data objectForKey:@"apparentTemperature"]) {
        object.apparentTemperature = [data objectForKey:@"apparentTemperature"];
    }
    
    if ([data objectForKey:@"dewPoint"]) {
        object.dewPoint = [data objectForKey:@"dewPoint"];
    }
    
    if ([data objectForKey:@"humidity"]) {
        object.humidity = [data objectForKey:@"humidity"];
    }
    
    if ([data objectForKey:@"dashboardIcon"]) {
        object.dashboardIcon = [data objectForKey:@"dashboardIcon"];
    }
    
    if ([data objectForKey:@"dashboardImg"]) {
        object.dashboardImg = [data objectForKey:@"dashboardImg"];
    }
    
    if ([data objectForKey:@"ozone"]) {
        object.ozone = [data objectForKey:@"ozone"];
    }
    
    if ([data objectForKey:@"precipIntensity"]) {
        object.precipIntensity = [data objectForKey:@"precipIntensity"];
    }
    
    if ([data objectForKey:@"precipProbability"]) {
        object.precipProbability = [data objectForKey:@"precipProbability"];
    }
    
    if ([data objectForKey:@"precipType"]) {
        object.precipType = [data objectForKey:@"precipType"];
    }
    
    if ([data objectForKey:@"pressure"]) {
        object.pressure = [data objectForKey:@"pressure"];
    }
    
    if ([data objectForKey:@"summary"]) {
        object.summary = [data objectForKey:@"summary"];
    }
    
    if ([data objectForKey:@"sunriseTime"]) {
        object.sunriseTime = [data objectForKey:@"sunriseTime"];
    }
    
    if ([data objectForKey:@"sunsetTime"]) {
        object.sunsetTime = [data objectForKey:@"sunsetTime"];
    }
    
    if ([data objectForKey:@"temperature"]) {
        object.temperature = [data objectForKey:@"temperature"];
    }
    
    if ([data objectForKey:@"time"]) {
        object.time = [data objectForKey:@"time"];
    }
    
    if ([data objectForKey:@"windBearing"]) {
        object.windBearing = [data objectForKey:@"windBearing"];
    }
    
    if ([data objectForKey:@"windSpeed"]) {
        object.windSpeed = [data objectForKey:@"windSpeed"];
    }
    
    if ([data objectForKey:@"apparentTemperatureMax"]) {
        object.apparentTemperatureMax = [data objectForKey:@"apparentTemperatureMax"];
    }
    
    if ([data objectForKey:@"apparentTemperatureMaxTime"]) {
        object.apparentTemperatureMaxTime = [data objectForKey:@"apparentTemperatureMaxTime"];
    }
    
    if ([data objectForKey:@"apparentTemperatureMin"]) {
        object.apparentTemperatureMin = [data objectForKey:@"apparentTemperatureMin"];
    }
    
    if ([data objectForKey:@"apparentTemperatureMinTime"]) {
        object.apparentTemperatureMinTime = [data objectForKey:@"apparentTemperatureMinTime"];
    }
    
    if ([data objectForKey:@"moonPhase"]) {
        object.moonPhase = [data objectForKey:@"moonPhase"];
    }
    
    if ([data objectForKey:@"precipIntensityMax"]) {
        object.precipIntensityMax = [data objectForKey:@"precipIntensityMax"];
    }
    
    if ([data objectForKey:@"precipIntensityMaxTime"]) {
        object.precipIntensityMaxTime = [data objectForKey:@"precipIntensityMaxTime"];
    }
    
    if ([data objectForKey:@"temperatureMinTime"]) {
        object.temperatureMinTime = [data objectForKey:@"temperatureMinTime"];
    }
    
    if ([data objectForKey:@"iconImg"]) {
        object.iconImg = [data objectForKey:@"iconImg"];
    }
    
    
    if ([data objectForKey:@"temperatureMax"]) {
        object.temperatureMax = [data objectForKey:@"temperatureMax"];
    }
    
    if ([data objectForKey:@"temperatureMaxTime"]) {
        object.temperatureMaxTime = [data objectForKey:@"temperatureMaxTime"];
    }
    
    if ([data objectForKey:@"temperatureMin"]) {
        object.temperatureMin = [data objectForKey:@"temperatureMin"];
    }
    
    return object;
}

@end
